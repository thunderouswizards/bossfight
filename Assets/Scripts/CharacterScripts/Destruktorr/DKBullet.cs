﻿using UnityEngine;

/// <summary>
/// Projectile behavior
/// </summary>
public class DKBullet : BulletScript
{
    private GameObject enemy;
    public int TimeToLive;
    public int make;
    public int stallStart;

    new public void Start()
    {
        make = 0;
    }

    public void setEnemy(GameObject enemy)
    {
        this.enemy = enemy;
    }

    public new void Update()
    {
        // 2 - Movement
        movement = new Vector2(
            speed.x * direction.x,
            speed.y * direction.y);

        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);

        if ((screenPosition.x > Screen.width || screenPosition.x < 0) ||
            (screenPosition.y > Screen.height || screenPosition.y < 0))
            Destroy(this.gameObject);

        if (stallStart > 0)
            stallStart--;

        if(stallStart <= 0)
            make++;
    }

    public new void FixedUpdate()
    {
        // Apply movement to the rigidbody
        if (stallStart <= 0)
        {
            GetComponent<Rigidbody2D>().velocity = movement;
        }
        if (make >= TimeToLive)
        {
            Destroy(this.gameObject);
        }

    }

    public new void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(owner))
            {
                Player script1 = col.gameObject.GetComponent<Player>();
                //Player2 script2 = col.gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible)
                    {
                        script1.StunHit(120);
                        Destroy(this.gameObject);
                    }
                }
                /*if(script2.enabled){
					if(!script2.invincible){
						Vector3 dir = (col.transform.position - transform.position).normalized;
						dir.y = 1;
						col.attachedRigidbody.velocity = dir * 13.75f;
						script2.ProjectileHit();
						Destroy (this.gameObject);
					}
				}*/
            }
        }
        else if (col.gameObject.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }
}
