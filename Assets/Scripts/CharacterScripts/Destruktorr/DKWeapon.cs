﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Launch projectile
/// </summary>
public class DKWeapon : WeaponScript
{
    private int currentBullet = 0;
    public FocusCamera mainCamera;
    public Player p1;
    public Vector3 airTarget;
    public Vector3 groundTarget;
    public bool isMoving;
    public bool startStomp;
    public int WaitPunchGoal;
    private int ResetTime;
    public int timer;

    new public void Start()
    {
        maxBullets = 4;
        base.Start();
        timer = 0;
    }

    new public void Update()
    {
        if (shootCooldown > 0)
        {
            shootCooldown -= 1;
        }

        if (altCooldown > 0)
        {
            altCooldown -= 1;
        }

        if (bullets.Count > 0)
        {
            for (int i = 0; i < bullets.Count; i++)
            {
                if (bullets[i] == null)
                    bullets.Remove(bullets[i]);
            }

            if(bullets.Count > 0)
            {
                if (bullets[0] != null)
                {
                    if (bullets[0].GetComponent<Rigidbody2D>().velocity.x == 0)
                    {
                        p1.StopMovement(60);
                        p1.stunned = true;
                        if (p1.isAirborne())
                            p1.animate.SetBool("Grounded", true);
                        else
                            p1.animate.SetBool("Grounded", false);
                    }

                   
                }
                timer++;

                if (timer == WaitPunchGoal)
                {
                    p1.animate.SetTrigger("PUNCH");
                }
            }

            /*if(timer < WaitPunchGoal)
                timer++;

            if (timer == WaitPunchGoal)
                p1.animate.SetTrigger("PUNCH");*/
            
        }

        if (isMoving)
        {
            p1.SetGrav(0);
            if (!startStomp)
            {
                if (p1.MadeContact())
                {
                    airTarget = p1.transform.position;
                }

                MoveTowardTarget(airTarget);

                if (!isMoving)
                {
                    SetGroundTarget();

                    isMoving = true;
                    MoveTowardTarget(groundTarget);
                    /*//p1.animate.Play("StartFall", -1, 0f);
                    p1.animate.SetBool("StartFall", true);
                    p1.animate.SetBool("StartJump", false);*/
                    startStomp = true;
                }
            }
            else if (startStomp)
            {
                SetGroundTarget();

                MoveTowardTarget(groundTarget);
                if (!isMoving)
                {
                    GroundStompStun();
                    startStomp = false;
                }
            }
        }

        if (timer == ResetTime)
            timer = 0;

    }

    new public void Attack(bool isEnemy, bool faceRight, float altFire)
    {
        if (CanAttack && altFire != 1)
        {
            p1.animate.SetTrigger("Stomp");
            for (int i = 0; i < 4; i++)
            {

                shootCooldown = shootingRate;

                // Create a new shot
                var shotTransform = Instantiate(shotPrefab) as Transform;

                // Assign position
                if (faceRight)
                    shotTransform.position = new Vector3(transform.position.x + 2.0f, transform.position.y, transform.position.z);
                else
                    shotTransform.position = new Vector3(transform.position.x - 2.0f, transform.position.y, transform.position.z);

                // The is enemy property
                DKBullet shot = shotTransform.gameObject.GetComponent<DKBullet>();
                ResetTime = shot.TimeToLive + shot.stallStart;


                if (shot != null)
                {
                    shot.setOwner(gameObject);

                    /*if (faceRight)
                        shot.direction = transform.right; // towards in 2D space is the right of the sprite
                    else
                        shot.direction = transform.right * -1;*/
                }
                bullets.Add(shot);
            }

            if (faceRight)
            {
                bullets[0].direction = new Vector2(this.transform.right.x, 1);
                bullets[1].direction = new Vector2(this.transform.right.x, 0);
                bullets[2].direction = new Vector2(this.transform.right.x, -0.5f);
                bullets[3].direction = new Vector2(this.transform.right.x, 0.5f);
            }
            else
            {
                bullets[0].direction = new Vector2(this.transform.right.x * -1, 1);
                bullets[1].direction = new Vector2(this.transform.right.x * -1, 0);
                bullets[2].direction = new Vector2(this.transform.right.x * -1, -0.5f);
                bullets[3].direction = new Vector2(this.transform.right.x * -1, 0.5f);
            }
        }

        if (CanAlt && altFire == 1)
        {
            altCooldown = altRate;

            //mainCamera.shakeDuration = 0.3f;
            airTarget.y = p1.transform.position.y + 5.0f;
            airTarget.z = p1.transform.position.z;

            if (faceRight)
                airTarget.x = this.transform.position.x + 5.0f;
            //new Vector3(this.transform.position.x + 3f,this.transform.position.y,this.transform.position.z);
            if (!faceRight)
                airTarget.x = this.transform.position.x - 5.0f;

            isMoving = true;

            p1.velocity.x = 0;
            p1.velocity.y = 0;

            p1.SetGrav(0);

            //p1.animate.Play("Jump", -1, 0f);
            if (!p1.isAirborne())
            {
                p1.StartJump();
                MoveTowardTarget(airTarget);
            }
            else if (p1.isAirborne())
            {
                SetGroundTarget();
                startStomp = true;
                MoveTowardTarget(groundTarget);
            }
        }

    }


    public void MoveTowardTarget(Vector3 target)
    {
        if (Vector3.Distance(p1.transform.position, target) >= 1.0f)
        {
            Vector2 directionOfTravel = target - p1.transform.position;
            //now normalize the direction, since we only want the direction information
            directionOfTravel.Normalize();
            //scale the movement on each axis by the directionOfTravel vector components
            if(startStomp)
                p1.velocity = directionOfTravel * (1.5f * p1.jumpVelocity);
            else
                p1.velocity = directionOfTravel * (0.5f * p1.jumpVelocity);
        }
        else
        {
            isMoving = false;
            p1.SetGrav(-(2 * p1.jumpHeight) / Mathf.Pow(p1.timeToJumpApex, 2));
        }
    }

    public void GroundStompStun()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++)
        {
            //if (!players[i].name.Equals(this.gameObject.name))
            if(players[i] != this.gameObject)
            {
                Player script1 = players[i].gameObject.GetComponent<Player>();
                //Player2 script2 = players[i].gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible && !script1.isAirborne())
                    {
                        script1.StunHit(180);
                    }
                }
                /*if (script2.enabled)
                {
                    if (!script2.invincible)
                    {
                        script2.StunHit(180);
                    }
                }*/
            }
        }
    }
    public void SetGroundTarget()
    {
        groundTarget.x = p1.transform.position.x;
        groundTarget.y = p1.transform.position.y - 10000;
        groundTarget.z = p1.transform.position.z;

        if (p1.HitsTheFloor())
        {
            groundTarget = p1.transform.position;
        }
    }

}