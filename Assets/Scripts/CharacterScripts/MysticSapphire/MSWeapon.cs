﻿using UnityEngine;
using System.Collections;

public class MSWeapon : WeaponScript {

    private int currentBullet = 0;

    new public void Start()
    {
        maxBullets = 2;
        base.Start();
    }

    new public void Update()
    {
        if (bullets.Count == 1)
            currentBullet = 0;
        base.Update();
    }

    new public void Attack(bool isEnemy, bool faceRight, float altFire)
    {

        if (CanAttack && altFire != 1)
        {
            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;

            // The is enemy property
            MSBullet shot = shotTransform.gameObject.GetComponent<MSBullet>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            if (shot != null)
            {
                shot.setOwner(gameObject);
                if (gameObject.GetComponent<MSAI>().isActiveAndEnabled)
                    gameObject.GetComponent<MSAI>().AddBullet(shot.transform);

                if (faceRight)
                    shot.direction = transform.right; // towards in 2D space is the right of the sprite
                else
                    shot.direction = transform.right * -1;
            }

            bullets.Add(shot);
        }

        if (altFire == 1 && bullets.Count >= currentBullet+1)
        {
            if (((MSBullet)bullets[currentBullet]).IsStopped())
            {
                gameObject.GetComponents<AudioSource>()[6].Play();

                ((MSBullet)bullets[currentBullet]).GoBack();
                if (bullets.Count == 2)
                {
                    if (currentBullet == 0)
                        currentBullet = 1;
                    else
                        currentBullet = 0;
                }
            }
            else
            {
                gameObject.GetComponents<AudioSource>()[4].Play();

                ((MSBullet)bullets[currentBullet]).Stay();
                if(bullets.Count == 2) {
                    if (currentBullet == 0)
                        currentBullet = 1;
                    else
                        currentBullet = 0;
                }
            }
        }

    }
}
