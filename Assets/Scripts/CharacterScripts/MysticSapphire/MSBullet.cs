﻿using UnityEngine;
using System.Collections;

public class MSBullet : BulletScript {

    public Vector2 stopped = new Vector2(0.0f, 0.0f);
    public Vector2 oldSpeed;
    public Vector3 returnPos;
    public bool goingBack;
    public int timeToLive;

    public Vector3 rotateTarget;
    private bool targetSet;
    private bool rotate;
    private float rotateAngle;
    private bool groundedPlayer;

    new public void Start()
    {
        goingBack = false;
        speed = new Vector2(30.0f, 0.0f);
        rotateAngle = 180.0f;
        targetSet = false;
        rotate = true;
        groundedPlayer = true;
    }

    new public void Update()
    {
        if (goingBack)
        {
            transform.position = Vector3.MoveTowards(transform.position, returnPos, speed.x * Time.deltaTime);
        }

        else if(rotate) {
            if(!targetSet)
            {
                groundedPlayer = getOwner().GetComponent<Player>().IsGrounded();
                //If going right, we want RotateAngle = -180 and rotateTarget.x = transform.position.x + 5.0f
                //If going left, we want RotateAngel = 180 and rotateTarget.x = transorm.position - 5.0f

                if (direction.x == transform.right.x)
                {
                    if (groundedPlayer)
                    {
                        //if grounded, rotateAngle = -180
                        rotateAngle = -180.0f;
                    }
                    else
                    {
                        rotateAngle = 180.0f;
                    }
                    rotateTarget = new Vector3(transform.position.x + 7.5f, transform.position.y, 0.0f);
                }
                else
                {
                    if (groundedPlayer)
                    {
                        //if grounded, rotateAngle = -180
                        rotateAngle = 180.0f;
                    }
                    else
                    {
                        rotateAngle = -180.0f;
                    }

                    rotateTarget = new Vector3(transform.position.x - 7.5f, transform.position.y, 0.0f);
                }
                targetSet = true;
            }

            transform.RotateAround(rotateTarget, new Vector3(0.0f,0.0f, 1.0f), rotateAngle * Time.deltaTime);

            if((transform.position.y < rotateTarget.y && groundedPlayer) || 
                (transform.position.y > rotateTarget.y && !groundedPlayer))
            {
                Stay();
                GoBack();
            }

            /*movement = new Vector2(
                speed.x * direction.x,
                speed.y * direction.y);*/

            /*Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            if ((screenPosition.x > Screen.width || screenPosition.x < 0) ||
                (screenPosition.y > Screen.height || screenPosition.y < 0))
                Destroy(this.gameObject);
                */
        }
    }

    new public void FixedUpdate()
    {
        // Apply movement to the rigidbody
        //GetComponent<Rigidbody2D>().velocity = movement;
    }

    public void Stay()
    {
        oldSpeed = speed;
        speed = stopped;

        rotate = false;
    }

    public void GoBack()
    {
        speed = oldSpeed;
        returnPos = getOwner().transform.position;
        returnPos = returnPos - transform.position;
        returnPos = transform.position + returnPos.normalized * 300.0f;
        /*if (ownPos.x > transform.position.x)
            direction.x = 1;
        else if (ownPos.x < transform.position.x)
            direction.x = -1;

        if (ownPos.y > transform.position.y)
            direction.y = 1;
        else if (ownPos.y < transform.position.y)
            direction.y = -1;*/

        goingBack = true;
    }

    public bool IsStopped()
    {
        if (speed.x == stopped.x)
            return true;
        else
            return false;
    }


    new public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(getOwner()))
            {
                Player script1 = col.gameObject.GetComponent<Player>();
                //Player2 script2 = col.gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible)
                    {
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 13.75f;
                        script1.ProjectileHit();
                        RemoveFromAIList();
                        Destroy(this.gameObject);
                    }
                }
                /*if (script2.enabled)
                {
                    if (!script2.invincible)
                    {
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 13.75f;
                        script2.ProjectileHit();
                        Destroy(this.gameObject);
                    }
                }*/
            }

            if (col.gameObject.Equals(getOwner()) && goingBack)
            {
                RemoveFromAIList();
                Destroy(this.gameObject);
            }
        }
        else if (col.gameObject.tag == "Wall")
        {
            RemoveFromAIList();
            Destroy(this.gameObject);
        }
    }

    public void RemoveFromAIList()
    {
        if(owner.GetComponent<MSAI>().isActiveAndEnabled)
        {
            owner.GetComponent<MSAI>().RemoveBullet(transform);
        }
    }

}
