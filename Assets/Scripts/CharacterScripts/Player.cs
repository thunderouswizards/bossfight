﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {

    public float health;

    public Material[] costumes;
    public SkinnedMeshRenderer outfit;
    public GameObject AIBumper;

    public TrailRenderer dashTrail;
    public GameObject dust;
    private bool dustKicked = false;
    public Player opponent;
	//public GameObject[] Health;
    public GameObject Caution;
    public Image bar;
    private float hpValue;
    private float target;
    public bool isPlayer2;
	public float jumpHeight = 4;

	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	public float moveSpeed = 6;
	public float dashSpeed = 13.75f;
    public bool canDash = false;
	private bool isDashing = false;
	public bool invincible = false;
    private bool isHit = false;
    public bool stunned = false;
	public int invinCount = 0;
	private int hitCount = 0;
    private int stunCount = 0;
    private bool TouchingPlayer;
    public float DashTargetInc = 6.0f;
    
    private int winCount = 0;
    public int dashCoolDown = 0;
    public int dashDecrement = 1;
    //This is for characters whom deal damage with a dash.
    public bool isDasher;


    public GameObject model;
    public Animator animate;

	float gravity;
	public float jumpVelocity;

    private float ShortJumpV;
    public Vector3 velocity;
	float velocityXSmoothing;

    private bool rotationDone;
    private bool faceRight;
    private bool airborne;
    private bool grounded;
    private bool moving;


	//private Vector3 start;
	private Vector3 dashTarget;

    private string horizontalKey = "Horizontal";
    private string keyHKey = "KeyH";
    private string keyVKey = "KeyV";
    private string verticalKey = "Vertical";
    private string fireKey = "Fire";
    private string altKey = "AltFire";
    private string jumpKey = "Jump";
    private string dashKey = "Dash";
    private string pauseKey = "Start";

    private float oldSpeed;
	
	public Controller2D controller;

    private AudioSource[] sounds;
    private bool paused = false;

    private int slowTime = -1;
    private bool slow;
    private bool refill;

    public GameObject koText;

    private SkinnedMeshRenderer renderedModel;

    private Transform OrigParent;

    private AI AIScript;

    public bool isAI = false;

    private bool CanFastFall;


    private bool JumpCancel;
    private bool Jump;

    void Start() {
        Initialization();
    }

    public void Initialization()
    {
        OrigParent = this.transform.parent;

        controller = GetComponent<Controller2D>();

        rotationDone = false;

        TouchingPlayer = false;

        faceRight = true;
        airborne = true;
        grounded = false;
        moving = false;
        CanFastFall = false;

        //Short hop stuff:
        Jump = false;
        JumpCancel = false;
        ShortJumpV = jumpVelocity / 2;

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        oldSpeed = moveSpeed;

        hpValue = 1.0f;
        target = 1.0f;


        if (isPlayer2)
        {
            horizontalKey = "Horizontal2";
            keyHKey = "KeyH2";
            verticalKey = "Vertical2";
            keyVKey = "KeyV2";
            fireKey = "Fire2";
            altKey = "AltFire2";
            jumpKey = "Jump2";
            dashKey = "Dash2";
        }

        sounds = this.GetComponents<AudioSource>();
        slowTime = 0;
        slow = false;
        refill = false;

        renderedModel = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();

        if (this.GetComponent<AI>() != null)
        {
            if (this.GetComponent<AI>().enabled)
            {
                AIScript = this.GetComponent<AI>();
                AIBumper.SetActive(true);
                isAI = true;
            }
        }

    }
	
	void Update() {
        if (isAI)
            AIUp();
        else
            PlayerUp();

        if (slowTime > 0)
        {
            invincible = true;
            slowTime--;
        }

        else if (slowTime == 0 && slow)
        {
            invincible = false;
            Time.timeScale = 1;
            slow = false;
            slowTime = -1;

            if (hpValue <= 0.1f)
            {
                refill = true;
            }

            /*if (winCount == 2)
            {
                SelectManager sm = GameObject.FindGameObjectWithTag("CharacterLoader").GetComponent<SelectManager>();
                if (isPlayer2)
                    sm.SetPlayerWon(0);
                else
                    sm.SetPlayerWon(1);
                Application.LoadLevel("VictoryScreen");
            }*/
        }

        if (!isDashing && dashCoolDown > 0 && Time.timeScale != 0.0f)
        {
            dashCoolDown -= dashDecrement;
            if (dashCoolDown <= 0)
            {
                Caution.SetActive(false);
            }
        }

        if (controller.collisions.above)
        {
            velocity.y = 0;
        }

        if ((velocity.y > 0 || velocity.y < 0) && !controller.collisions.below)
        {
            airborne = true;
            grounded = false;
            dustKicked = false;
            
        }

        if (controller.collisions.below && !isDashing)
        {
            canDash = true;
            airborne = false;
            if (!grounded)
                sounds[2].Play();
            grounded = true;
            CanFastFall = true;
            velocity.y = -1.0f;
            KickUpDust();
        }
        else if (!controller.collisions.below)
        {
            velocity.y += gravity * Time.deltaTime;
        }

        if (hitCount > 0)
        {
            velocity.x = 0;
            velocity.y = 0;
            hitCount--;
        }
        else if (hitCount <= 0)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }

        if (invinCount > 0)
        {
            invinCount--;
            if (isHit)
            {
                renderedModel.enabled = !(renderedModel.enabled);
            }
        }
        else if (invinCount <= 0)
        {
            isHit = false;
            invincible = false;
            renderedModel.enabled = true;
        }

        if (stunCount > 0)
        {
            moveSpeed = 0;
            stunCount--;
            velocity.x = 0;
            velocity.y = 0;
            gravity = 0;
            if (stunCount <= 0)
            {
                invinCount = 60;
                invincible = true;
            }
        }
        else if (stunCount <= 0)
        {
            gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            moveSpeed = oldSpeed;

            //Let's not change stunned unless we have to
            if (stunned == true)
            {
                stunned = false;
                animate.SetBool("Stunned", false);
            }
            //stunned = false;
        }

        if (hitCount > 0)
        {
            stunCount = 0;
        }

        animate.SetFloat("xVelocity", Mathf.Abs(velocity.x));
        animate.SetFloat("yVelocity", velocity.y);
        //UpdateGraphics();
    }
    void PlayerUp()
    {

        Vector2 input = SetInput(new Vector2(0.0f, 0.0f));

        if (!paused && !isDashing && !slow)
        {
            input = ReadMoveInput(false, Input.GetAxisRaw(horizontalKey), Input.GetAxisRaw(verticalKey));
            //new Vector2(Input.GetAxisRaw(horizontalKey), Input.GetAxisRaw(verticalKey));
            if (input.x == 0 && input.y == 0)
            {
                input = new Vector2(Input.GetAxisRaw(keyHKey), Input.GetAxisRaw(keyVKey));
            }
        }

        if (Input.GetButtonDown(jumpKey) && controller.collisions.below && !stunned && !paused && !isDashing && !slow)
        {
            Jump = true;
            //StartJump();
        }
        if (Input.GetButtonUp(jumpKey) && !controller.collisions.below && !stunned && !paused && !isDashing && !slow)
        {
            JumpCancel = true;
        }

        if (Input.GetButtonDown(dashKey) && !isDashing && canDash && !stunned && dashCoolDown <= 0 && !paused && !slow)
        {
            Vector2 DashInput = new Vector2(Input.GetAxisRaw(horizontalKey), Input.GetAxisRaw(verticalKey));
            Vector2 DashKeyInput = new Vector2(Input.GetAxisRaw(keyHKey), Input.GetAxisRaw(keyVKey));
            DashPressed(DashInput, DashKeyInput);
        }

        
        float targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        controller.Move(velocity * Time.deltaTime);

        if (gameObject.name != "MysticSapphire(Clone)")
        {
            if ((input.x > 0.0f || input.x < 0.0f) && !sounds[6].isPlaying && grounded)
            {
                sounds[6].Play();
            }
            else if ((input.x == 0.0f && sounds[6].isPlaying) || !grounded)
            {
                sounds[6].Stop();
            }
        }

        // 5 - Shooting
        float altFire = 0;
        bool shoot = Shoot();
        if (Input.GetButtonDown(altKey))
        {
            altFire = AltPressed();
        }

        if ((shoot || altFire == 1) && !isDashing && hitCount == 0 && !stunned && !paused && !slow)
        {

            if (controller.collisions.below)
            {
                if (altFire == 1 && this.GetComponent<WeaponScript>().CanAlt)
                    animate.SetTrigger("AltShoot");
                else if (altFire != 1 && this.GetComponent<WeaponScript>().CanAttack && gameObject.name != "KingMaximum(Clone)")
                    animate.SetTrigger("Shoot");

            }
            if (!controller.collisions.below)
            {
                if (altFire == 1 && this.GetComponent<WeaponScript>().CanAlt)
                    animate.SetTrigger("AltShoot");
                else if (altFire != 1 && this.GetComponent<WeaponScript>().CanAttack && gameObject.name != "KingMaximum(Clone)")
                    animate.SetTrigger("Shoot");

            }
            WeaponScript weapon = GetComponent<WeaponScript>();
            if (weapon != null)
            {
                // false because the player is not an enemy
                if (weapon is MSWeapon)
                    ((MSWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is DKWeapon)
                    ((DKWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is KMWeapon)
                    ((KMWeapon)weapon).Attack(false, altFire);
                else if (weapon is RWeapon)
                    ((RWeapon)weapon).Attack(false, faceRight, altFire, input.x, input.y);
                else if (weapon is TKWeapon)
                    ((TKWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is STWeapon)
                    ((STWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is SSWeapon)
                    ((SSWeapon)weapon).Attack(false, faceRight, altFire);
                else
                    weapon.Attack(false, faceRight, altFire);
            }

            if (altFire != 1 && !(weapon is STWeapon))
                sounds[3].Play();
            else if(altFire == 1 && !(weapon is MSWeapon))
                sounds[4].Play();
        }

        if (input.x != 0 && grounded && !isDashing && !stunned)
        {
            //animate.SetBool("IsRun", true);
            //animate.SetBool("IsIdle", false);
            moving = true;
        }

        else if ((Input.GetAxisRaw(horizontalKey) == 0 || Input.GetAxisRaw(keyHKey) == 0) && !stunned)
        {
            //animate.SetBool("IsRun", false);
            //animate.SetBool("IsIdle", true);
            moving = false;
        }

        if (input.x > 0 && !isDashing && hitCount == 0 && stunCount == 0)
        {
            faceRight = true;
        }

        if (input.x < 0 && !isDashing && hitCount == 0 && stunCount == 0)
        {
            faceRight = false;
        }

        if(input.y < 0 && !isDashing && hitCount == 0 && stunCount == 0 && airborne && CanFastFall)
        {
            if (velocity.y <= 0.001f)
            {
                velocity.y = 0;
                velocity.y += (gravity * Time.deltaTime);
                velocity.y *= 8;
                CanFastFall = false;
            }
        }

        

        if (Input.GetButtonDown(pauseKey) && !isPlayer2 && Time.timeScale != 0.25f)
        {
            if (paused)
            {
                paused = false;
                Time.timeScale = 1;
            }
            else if (!paused && !slow)
            {
                paused = true;
                Time.timeScale = 0;
            }
        }

        if (Input.GetKeyDown("escape") && paused)
        {
            Application.Quit();
        }

    }

    void AIUp()
    {
        float targetVelocityX = 0;
        if (AIScript.MoveLeft())
            targetVelocityX = -1 * moveSpeed;

        else if (AIScript.MoveRight())
            targetVelocityX = 1 * moveSpeed;

        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if (AIScript.Jump() && controller.collisions.below && !stunned && !paused && !isDashing && !slow)
            Jump = true;
            //StartJump();

        if ((AIScript.Attack() || AIScript.AltFire()) && !isDashing && hitCount == 0 && stunCount == 0 && !paused && !slow)
        {
            float altFire = 0;

            if (AIScript.AltFire())
            {
                //print("AltFire. Leggo.");
                altFire = 1;
            }
            else if (!AIScript.AltFire())
            {
                altFire = -1;
            }

            if (controller.collisions.below)
            {
                if (altFire == 1 && this.GetComponent<WeaponScript>().CanAlt)
                    animate.SetTrigger("AltShoot");

                else if (altFire != 1 && this.GetComponent<WeaponScript>().CanAttack)
                    animate.SetTrigger("Shoot");
            }

            if (!controller.collisions.below)
            {
                if (altFire == 1 && this.GetComponent<WeaponScript>().CanAlt)
                    animate.SetTrigger("AltShoot");
                else if (altFire != 1 && this.GetComponent<WeaponScript>().CanAttack)
                    animate.SetTrigger("Shoot");

            }
            WeaponScript weapon = GetComponent<WeaponScript>();
            if (weapon != null)
            {
                // false because the player is not an enemy
                if (weapon is MSWeapon)
                    ((MSWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is DKWeapon)
                    ((DKWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is KMWeapon)
                    ((KMWeapon)weapon).Attack(false, altFire);
                else if (weapon is RWeapon)
                    ((RWeapon)weapon).Attack(false, faceRight, altFire, velocity.x, velocity.y);
                else if (weapon is TKWeapon)
                    ((TKWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is STWeapon)
                    ((STWeapon)weapon).Attack(false, faceRight, altFire);
                else if (weapon is SSWeapon)
                    ((SSWeapon)weapon).Attack(false, faceRight, altFire);
                else
                    weapon.Attack(false, faceRight, altFire);
            }

            if (altFire != 1)
                sounds[3].Play();
            else
                sounds[4].Play();
        }

        if (AIScript.Dash() && !isDashing && canDash && !stunned && dashCoolDown <= 0 && !paused && !slow)
        {
            DashPressed(AIScript.DashTarget(), new Vector2 (0,0));
        }

        if ((AIScript.MoveLeft() || AIScript.MoveRight()) && grounded && !isDashing && !stunned)
        {
            //animate.SetBool("IsRun", true);
            //animate.SetBool("IsIdle", false);
            moving = true;
        }

        else if (AIScript.Idle() && !stunned && grounded)
        {
            //animate.SetBool("IsRun", false);
            //animate.SetBool("IsIdle", true);
            moving = false;
        }



        if (velocity.x > 0 && !isDashing && hitCount == 0 && stunCount == 0)
        {
            faceRight = true;
        }

        if (velocity.x < 0 && !isDashing && hitCount == 0 && stunCount == 0)
        {
            faceRight = false;
        }
        
    }
    void FixedUpdate()
    {
        if (isDashing)
        {
            gravity = 0;
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.0f;
            //start = this.transform.position;

            if ((TouchingPlayer || controller.collisions.left || controller.collisions.right) || (!grounded && controller.collisions.below))
            {
                dashTarget = this.transform.position;
            }
           

            MoveTowardTarget();
        }


        if (faceRight && rotationDone)
        {
            model.transform.Rotate(0, 180, 0);
            rotationDone = false;
        }

        if (!faceRight && !rotationDone)
        {
            model.transform.Rotate(0, 180, 0);
            rotationDone = true;
        }

        if (grounded)
        {
            //animate.SetBool("StartJump", false);
            //animate.SetBool("StartFall", false);
            animate.SetBool("Grounded", true);
        }

        if (airborne)
        {
            animate.SetBool("Grounded", false);
            //animate.SetBool("IsIdle", false);
            //animate.SetBool("IsRun", false);
        }

        if (Jump)
        {
            StartJump();
            Jump = false;
        }

        if (JumpCancel)
        {
            if(velocity.y > ShortJumpV)
            {
                velocity.y = ShortJumpV;
            }
            JumpCancel = false;
        }

        UpdateGraphics();
    }

    public bool isAirborne()
    {
        return airborne;
    }

    public void MoveTowardTarget(){
        if (Vector3.Distance (transform.position, dashTarget) > 0.5f) { 
			
			Vector2 directionOfTravel = dashTarget - transform.position;
			
            //now normalize the direction, since we only want the direction information
			directionOfTravel.Normalize ();
            
            //scale the movement on each axis by the directionOfTravel vector components
            velocity = directionOfTravel * dashSpeed;

        }
        /*if (Vector3.Distance(transform.position, dashTarget) > 0.5f)
        {
            transform.position = Vector3.MoveTowards(transform.position, dashTarget, dashSpeed * Time.deltaTime);
        }*/
        else {
            //this.velocity = Vector3.zero;
			isDashing = false;
            if(dashTrail != null)
                dashTrail.enabled = false;
            if (controller.collisions.below)
                grounded = true;
            
            dashCoolDown = 60;
            animate.SetBool("Dashing", false);
            gravity = -(2 * jumpHeight) / Mathf.Pow (timeToJumpApex, 2);
            Caution.SetActive(true);

            gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.0f;
          
        }
	}

	public void ProjectileHit(){
        if (!isDashing)
        {
            if(GetComponent<RWeapon>() != null)
            {
                GetComponent<RWeapon>().EndStance();
            }

            invinCount = 120;
            invincible = true;
            hitCount = 5;
            isHit = true;
            LoseHealth();

            if (grounded)
            {
                animate.SetTrigger("Hit");
            }
            else if (airborne)
            {
                animate.SetTrigger("Hit");
            }
        }
    }

    public void StunHit(int length)
    {
        if(stunCount != 0)
        {
            return;
        }
        stunCount = length;
        stunned = true;
        if(isDashing)
            isDashing = false;

        animate.SetBool("Stunned", true);

        /*if (grounded)
        {
            animate.SetTrigger("Stunned");
        }
        else if (airborne)
        {
            animate.SetTrigger("Stunned");
        }*/
    }

	public void LoseHealth(){
        /*if (Health [0].activeInHierarchy) {
			Health [0].SetActive (false);
		} else if (Health [1].activeInHierarchy) {
			Health[1].SetActive (false);
		}else if (Health [2].activeInHierarchy) {
			Health[2].SetActive (false);
		}else {
			Health[2].SetActive (true);
			Health[1].SetActive (true);
			Health[0].SetActive (true);
		}*/

        GetHit(100.0f, 1 / health);

        /*if (isDasher)
        {
            GetHit(100.0f, 0.167f);
        }
        else
        {
            GetHit(100.0f, 0.33f);
        }*/
	}

    public bool getIsDashing()
    {
        return isDashing;
    }

    public bool getIfDasher()
    {
        return isDasher;
    }
    
    public void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            TouchingPlayer = true;
            if (this.isDasher && this.isDashing && !c.gameObject.GetComponent<Player>().getIsDashing())
            {
                if (!c.gameObject.GetComponent<Player>().invincible) {
                    c.gameObject.GetComponent<Player>().ProjectileHit();
                }
                //Vector3 direction = (dashTarget - start)*-1;
                Vector3 direction = (dashTarget - transform.position) * -1;
                //xthis.velocity = new Vector2(direction.x*1.5F, 0.0f);
                velocity = direction * 1.5f;
            }
        }

       /*if (isDashing)
        {
            dashTarget = this.transform.position;
            velocity = new Vector3();
        }*/
    }

    public void StopMovement(int time)
    {
        stunCount = time;
    }

    public void OnCollisionStay2D(Collision2D c)
    {
        if (c.gameObject.tag == "Platform")
        {
            transform.parent = c.transform;
        }
    }

    public void OnCollisionExit2D(Collision2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            TouchingPlayer = false;
        }

        if (c.gameObject.tag == "Platform")
        {
            transform.parent = OrigParent;
        }
    }

    public bool IsThisPlayer2()
    {
        return isPlayer2;
    }

    public void GetHit(float damage, float percent)
    {
        float w = bar.rectTransform.rect.width;
        Vector3 v = bar.rectTransform.localPosition;

        float blockDis = w / 18.0f; //Width of one block within the bar
        //float posChange = blockDis * 6.0f;
        float posChange = blockDis * (18.0f / health);
        /*if (percent == 0.167f)
        {
            posChange = blockDis * 3.0f;
        }*/
        //PosChange = block width * amount of damage in blocks. We will subtract the x value by this.

        if (bar.rectTransform.localPosition.x != w * -1)
        {
            bar.rectTransform.localPosition = new Vector3(v.x - posChange, v.y, v.z);
        }
        target = hpValue - damage * (percent / 100);
    }

    // Update is called once per frame
    void UpdateGraphics()
    {
        //hpValue = Mathf.Lerp(hpValue, target, 0.3f);
        hpValue = target;

        if(hpValue <= 0.1f && !refill && !slow)
        {
            sounds[5].Play();
            koText.SetActive(true);
            Time.timeScale = 0.25f;
            SetSlow();
            opponent.SetSlow();
        }

        else if (hpValue <= 0.1f && refill)
        {
            koText.SetActive(false);
            //This is for testing purposes. For actual gameplay, uncomment the 0.0f line
            if (winCount < 2)
            {
                //print("Won");
                hpValue = 1.0f;
                target = 1.0f;
                winCount++;
                bar.rectTransform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            }
            refill = false;

            //hpValue = 0.0f;
            if (winCount == 2)
            {
                SelectManager sm = GameObject.FindGameObjectWithTag("LoaderInfo").GetComponent<SelectManager>();
                if (isPlayer2)
                    sm.SetPlayerWon(0);
                else
                    sm.SetPlayerWon(1);
                SceneManager.LoadScene("VictoryScreen");
            }
            else
            {
                GameObject.FindGameObjectWithTag("CharacterLoader").GetComponent<RoundTracker>().DisplayText();
            }
            //die
        }
        //bar.fillAmount = hpValue;
    }

    public void SetOpponent(Player opp)
    {
        opponent = opp;
    }

    public void SetSlow()
    {
        slow = true;
        slowTime = 180;
    }

    public void SetOldSpeed(float speed)
    {
        oldSpeed = speed;
    }

    public void SetGrav(float grav)
    {
        gravity = grav;
    }

    public void StartJump()
    {
        grounded = false;
        airborne = true;
        controller.collisions.below = false;
        //animate.Play("Jump", -1, 0f);
        velocity.y = jumpVelocity;
        sounds[1].Play();
    }

    public bool MadeContact()
    {

        if (!isDashing && (TouchingPlayer || controller.collisions.above || controller.collisions.left || controller.collisions.right ))
        {
            return true;
        }
        else
            return false;
    }
    public bool HitsTheFloor()
    {
        if(controller.collisions.below || TouchingPlayer)
        {
            return true;
        }
        return false;
    }

    //Keys are pressed methods for players and AI use: Moving, dashing, jumping, firing, and alt firing

    public bool Shoot()
    {
        return Input.GetButtonDown(fireKey);
    }
    public int AltPressed()
    {
        return 1;
    }
    public void DashPressed(Vector2 DashInput, Vector2 KeyDashInput)
    {
        canDash = false;
        //velocity.x = 0;
        //velocity.y = 0;
        this.velocity = Vector3.zero;
        //start = this.transform.position;
        //animate.Play("Dashing", -1, 0f);
        dashTarget.z = this.transform.position.z;

        if (faceRight)
            dashTarget.x = this.transform.position.x + DashTargetInc;
        else if (!faceRight)
            dashTarget.x = this.transform.position.x - DashTargetInc;

        if (DashInput.y > 0 || KeyDashInput.y > 0)
        {
            dashTarget.y = this.transform.position.y + DashTargetInc;
            if (DashInput.x == 0 && KeyDashInput.x == 0)
                    dashTarget.x = this.transform.position.x;
        }
        else if (DashInput.y < 0 || KeyDashInput.y < 0)
        {
            dashTarget.y = this.transform.position.y - DashTargetInc;
            if (DashInput.x == 0 && KeyDashInput.x == 0 && !grounded)
                dashTarget.x = this.transform.position.x;
        }

        else if (DashInput.y == 0 || KeyDashInput.y == 0)
        {
            dashTarget.y = this.transform.position.y;
        }

        if (grounded && dashTarget.y <= this.transform.position.y)
        {
            dashTarget.y = this.transform.position.y;
        }

        animate.SetTrigger("Dash");
        //animate.Play("Dashing", -1, 0f);

        sounds[0].Play();

        dashTarget = new Vector3(dashTarget.x, dashTarget.y, dashTarget.z);
        //new Vector3(this.transform.position.x - 3f,this.transform.position.y,this.transform.position.z);
       
        isDashing = true;
        if(dashTrail != null)
            dashTrail.enabled = true;
        
        animate.SetBool("Dashing", true);
    }
    

    public Vector2 SetInput(Vector2 x)
    {
        return x;
    }

    public Vector2 ReadMoveInput(bool isAI, float x, float y)
    {
        return (new Vector2(x, y));     
    }
    public bool IsGrounded()
    {
        return grounded;
    }

    public void setAI(bool x)
    {
        isAI = x;
    }

    public bool FacingRight()
    {
        return faceRight;
    }

    private void KickUpDust()
    {
        if (!dustKicked && Time.timeScale != 0.25f)
        {
            GameObject dust1 = Instantiate(dust) as GameObject;
            GameObject dust2 = Instantiate(dust) as GameObject;
            float yVal = gameObject.GetComponent<BoxCollider2D>().bounds.size.y;
            yVal /= 2.0f;
            dust1.transform.position = new Vector3(transform.position.x, transform.position.y - yVal);
            dust2.transform.position = new Vector3(transform.position.x, transform.position.y - yVal);
            dust2.transform.Rotate(0.0f, 180.0f, 0.0f);
            dustKicked = true;
        }
    }

    public void ActivateAI()
    {
        AIScript = this.GetComponent<AI>();
        AIScript.enabled = true;
        AIBumper.SetActive(true);
        isAI = true;
    }

    public void ChangeCostume(int costumeI)
    {
        if(costumeI < costumes.Length)
        {
            Material[] m = outfit.materials;
            for(int i = 0; i < m.Length; i++)
            {
                m[i] = costumes[costumeI];
            }
            outfit.materials = m;
        }
            
    }

    public void StopActions()
    {
        moveSpeed = 0;
        velocity.x = 0;
        velocity.y = 0;
        gravity = 0;
    }
}