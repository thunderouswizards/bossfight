﻿using UnityEngine;
using System.Collections;

public class KMBlast : BulletScript {

    private float size = 3;
    private float final = 4;
    private KMWeapon source;

	// Use this for initialization
	new public void Start () {
        transform.position = getOwner().transform.position;
	}
	
	// Update is called once per frame
	new public void Update () {
        transform.localScale = Vector3.one * size;
        size += 50.0f * Time.deltaTime;
        if (size > final) {
            source.setBlastGone();
            Destroy(gameObject);
        }
    }

    public void SetSource(KMWeapon s)
    {
        source = s;
    }

    public void setScaleFactor(float num)
    {
        final += num;
    }

    new public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(owner))
            {
                Player script1 = col.gameObject.GetComponent<Player>();
                //Player2 script2 = col.gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible)
                    {
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 13.75f;
                        script1.ProjectileHit();
                        source.setBlastGone();
                        Destroy(this.gameObject);
                    }
                }
                /*if (script2.enabled)
                {
                    if (!script2.invincible)
                    {
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 13.75f;
                        script2.ProjectileHit();
                        source.setBlastGone();
                        Destroy(this.gameObject);
                    }
                }*/
            }
        }
    }
}
