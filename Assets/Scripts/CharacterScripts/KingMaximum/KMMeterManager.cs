﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KMMeterManager : MonoBehaviour {

    public GameObject crown;
    public Text level;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateMeter()
    {
        crown.SetActive(true);
        level.gameObject.SetActive(true);
    }

    public void ChangeLevel(float value)
    {
        if(value == 10.0f)
        {
            level.text = "LV. MAXIMUM!";
        }
        else
        {
            int v = Mathf.FloorToInt(value);
            level.text = "LV. " + v;
        }
    }

    public void Recharge()
    {
        level.text = "RECHARGING";
    }
}
