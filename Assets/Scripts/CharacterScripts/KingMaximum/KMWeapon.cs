﻿using UnityEngine;
using System.Collections;

public class KMWeapon : WeaponScript {

    public GameObject particles;
    public int absorbWindow;
    public int numAbsorbed;
    public Player p1;
    //public Player2 p2;
    //private bool useP1;
    private bool shooting;
    private bool charging;
    private int chargeCount;
	private string fireKey;

    private KMMeterManager meter;

    private bool aiShoot;
    private bool aiActive;

	// Use this for initialization
	new public void Start () {
        shooting = false;
        charging = false;
        maxBullets = 1;
        numAbsorbed = 0;
        absorbWindow = 0;
        shootingRate = 60.0f;
        chargeCount = 90;
        /*if (p1.isActiveAndEnabled) {
            useP1 = true;
        }*/
		if (p1.IsThisPlayer2()) {
			fireKey = "Fire2";
		}
		else{
			fireKey = "Fire";
		}

        /*else {
            useP1 = false;
        }*/

        GameObject[] meters = GameObject.FindGameObjectsWithTag("KMMeter");

        int mI = 0;
        //if (gameObject.GetComponent<Player>().isPlayer2)
        //{
            for(int i = 0; i < meters.Length; i++)
            {
                if(meters[i].name == "P2KMMeter" && gameObject.GetComponent<Player>().isPlayer2)
                    mI = i;
                if (meters[i].name == "P1KMMeter" && !gameObject.GetComponent<Player>().isPlayer2)
                    mI = i;
            }
        //}
        meter = GameObject.FindGameObjectsWithTag("KMMeter")[mI].GetComponent<KMMeterManager>();
        meter.ActivateMeter();

        aiShoot = false;
        aiActive = gameObject.GetComponent<Player>().isAI;
    }
	
	// Update is called once per frame
	new public void Update () {
        if (absorbWindow > 0)
        {
            absorbWindow--;
            p1.invincible = true;
            /*if (useP1)
            {
                p1.invincible = true;
            }*/
            /*else
            {
                p2.invincible = true;
            }*/
        }
        else if (charging)
        {
            chargeCount--;
            if (chargeCount == 0 && numAbsorbed != 10)
            {
                numAbsorbed++;
                chargeCount = 90; var m = particles.GetComponent<ParticleSystem>().main;
                m.startSpeed = numAbsorbed;
                meter.ChangeLevel(numAbsorbed);
            }
        }
        else if (shooting)
        {
            p1.invincible = true;
            p1.StopMovement(60);
            /*if (useP1)
            {
                p1.invincible = true;
                p1.StopMovement(60);
            }*/
            /*else
            {
                p2.invincible = true;
                p2.StopMovement(60);
            }*/
        }
        else
        {
            if (/*useP1 && */p1.invinCount <= 0.0f)
            {
                p1.invincible = false;
                p1.invinCount = 0;
            }
            /*else
            {
                p2.invincible = false;
                p2.invinCount = 0;
            }*/
        }


        if (((Input.GetButtonUp(fireKey) && !aiActive) || (aiShoot && aiActive)) && numAbsorbed >= 1 && charging)
        {
            gameObject.GetComponent<Player>().animate.Play("ShootingGrounded", -1, 0f);

            var m = particles.GetComponent<ParticleSystem>().main;
            m.startSpeed = 1;

            particles.SetActive(false);

            shooting = true;
            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;

            // The is enemy property
            KMBlast shot = shotTransform.gameObject.GetComponent<KMBlast>();
            shot.SetSource(this);

            if (shot != null)
            {
                shot.setOwner(gameObject);
                shot.setScaleFactor(2 * numAbsorbed);
            }

            if (/*useP1 &&*/ numAbsorbed > 0)
            {
                p1.StopMovement(300);
                p1.invinCount = 300;
            }

            numAbsorbed = 0;
            charging = false;
            meter.Recharge();

            aiShoot = false;
        }

        shootCooldown--;

        if(shootCooldown == 0)
        {
            meter.ChangeLevel(0);
        }
	}

    public void Attack(bool isEnemy, float altFire)
    {
        if(altFire == 1 && absorbWindow <= 0.0f && shootCooldown <= 0.0f && !charging)
        {
            shootCooldown = shootingRate;
            absorbWindow = 30;
            p1.StopMovement((int)absorbWindow);
            p1.invinCount = (int)absorbWindow;
            /*if (useP1)
            {
                p1.StopMovement((int)absorbWindow);
                p1.invinCount = (int)absorbWindow;
            }*/
            /*else
            {
                p2.StopMovement((int)absorbWindow);
                p2.invinCount = (int)absorbWindow;
            }*/
        }

        else if(shootCooldown<=0)
        {
            charging = true;
            particles.SetActive(true);
            var m = particles.GetComponent<ParticleSystem>().main;
            m.startSpeed = 1;
            /*shooting = true;
            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;

            // The is enemy property
            KMBlast shot = shotTransform.gameObject.GetComponent<KMBlast>();
            shot.SetSource(this);
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            if (shot != null)
            {
                shot.setOwner(gameObject);
                shot.setScaleFactor(2*numAbsorbed);
            }

            if (useP1 && numAbsorbed > 0)
            {
                p1.StopMovement(300);
                p1.invinCount = 300;
            }*/
            /*else
            {
                p2.StopMovement(300);
                p2.invinCount = 300;
            }*/

            //numAbsorbed = 0.0f;
        }
    }

    public void setBlastGone()
    {
        shooting = false;
        p1.StopMovement(0);
        p1.invinCount = 0;
        /*if (useP1)
        {
            p1.StopMovement(0);
            p1.invinCount = 0;
        }*/
        /*else
        {
            p2.StopMovement(0);
            p2.invinCount = 0;
        }*/
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Projectile" && absorbWindow > 0)
        {
            /*if(numAbsorbed != 10)
            {
                numAbsorbed++;
            }
            Destroy(col.gameObject);*/
            BulletScript b = col.gameObject.GetComponent<BulletScript>();
            b.direction.x *= -1;
            b.setOwner(this.gameObject);
        }
    }

    public void shootForAi()
    {
        aiShoot = true;
    }
    
    public void startCharging()
    {
        charging = true;
    }

    public int GetAbsorbWindow()
    {
        return absorbWindow;
    }
}
