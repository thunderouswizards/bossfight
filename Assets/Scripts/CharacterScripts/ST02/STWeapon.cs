﻿using UnityEngine;
using System.Collections.Generic;

public class STWeapon : WeaponScript
{
    public Player p1;
    private bool useP1;
    private bool shooting;
    public Transform TrapPrefab;
    public int maxTraps = 3;

    public List<STTrap> traps = new List<STTrap>();
    
    private bool aiActive;

    // Use this for initialization
    new public void Start()
    {
        shooting = false;
        maxBullets = 1;
        shootingRate = 10.0f;
        if (p1.isActiveAndEnabled)
        {
            useP1 = true;
        }
        else {
            useP1 = false;
        }
        
    }

    // Update is called once per frame
    new public void Update()
    {
        if (shootCooldown > 0)
        {
            shootCooldown -= 1;
        }

        if (shooting)
        {
            p1.StopMovement(60);
            p1.stunned = true;
        }
        if (!shooting && bullets.Count > 0)
        {
            shooting = true;
        }

		for (int i = 0; i < traps.Count; i++) {
			if (traps [i] == null) 
				traps.Remove (traps [i]);
		}

    }

    public void setReticleGone()
    {
        shooting = false;
        if (useP1)
        {
            p1.StopMovement(0);
        }
        p1.animate.SetBool("Shooting", false);
    }

    new public void Attack(bool isEnemy, bool faceRight, float altFire)
    {
        if (altFire == 1 && traps.Count == 3)
        {
			Destroy(traps[0].gameObject);
            traps.Remove(traps[0]);
        }

        if (CanAttack && altFire != 1 && gameObject.GetComponent<Player>().IsGrounded())
        {
            p1.animate.SetBool("Shooting", true);

            shooting = true;

            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;
            //var trapTransform = Instantiate(TrapPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;
            //trapTransform.position = transform.position;

            STBullet shot = shotTransform.gameObject.GetComponent<STBullet>();
            // STTrap trap = shotTransform.gameObject.GetComponent<STTrap>();
            shot.AIactive(gameObject.GetComponent<Player>().isAI);

            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            if (shot != null)
            {
                shot.setOwner(this.gameObject);
                //trap.setOwner(this.gameObject);
                shot.SetWeaponSource(this);
                if (faceRight && altFire == 1)
                    //shot.direction = this.transform.right; // towards in 2D space is the right of the sprite
                    shot.transform.position = new Vector3(shot.transform.position.x + 10.0f, shot.transform.position.y, shot.transform.position.z);
                else
                    shot.transform.position = new Vector3(shot.transform.position.x - 10.0f, shot.transform.position.y, shot.transform.position.z);
            }
            //bullets.Add(shot);
        }
        if (CanTrap)
        {
            if (altFire == 1 && gameObject.GetComponent<Player>().IsGrounded())
            {
                var trapTransform = Instantiate(TrapPrefab) as Transform;
                trapTransform.position = transform.position;

                STTrap trap = trapTransform.gameObject.GetComponent<STTrap>();

                trap.setOwner(this.gameObject);

                trap.transform.position = this.transform.position;

                traps.Add(trap);
            }
        }
    }

    public bool CanTrap
    {
        get
        {
            return shootCooldown <= 0f && traps.Count < maxTraps;
        }
    }

}
