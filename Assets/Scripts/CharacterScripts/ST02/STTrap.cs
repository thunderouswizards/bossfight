﻿using UnityEngine;
using System.Collections;

public class STTrap : BulletScript
{
    // Use this for initialization
    new public void Start()
    {
        transform.position = getOwner().transform.position;
    }

    new public void Update()
    {

    }

    new public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(owner))
            {
                Player script1 = col.gameObject.GetComponent<Player>();
                //Player2 script2 = col.gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible)
                    {
                        script1.StunHit(240);
                        if (owner.GetComponent<Player>().isAI)
                            owner.GetComponent<STAI>().startShoot();

                        Destroy(this.gameObject);
                    }
                }
                /*if (script2.enabled)
                {
                    if (!script2.invincible)
                    {
                        script2.StunHit(240);
                        Destroy(this.gameObject);
                    }
                }*/
            }
        }
    }
}
