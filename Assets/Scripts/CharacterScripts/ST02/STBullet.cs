﻿using UnityEngine;
using System.Collections;

public class STBullet : BulletScript
{
    private STWeapon source;
    bool canMove;
    Vector3 velocity;
    float velocityXSmoothing;
    int moveSpeed = 10;
    bool letGo;
    bool inRange;
    Player hitP1 = null;
    private string fire;

    private bool ai;
    private int aiTimer;

    // Use this for initialization
    new public void Start()
    {
        transform.position = new Vector3(getOwner().transform.position.x, getOwner().transform.position.y, 0.0f);
        canMove = true;
        letGo = false;
        fire = "Fire";
    }

    // Update is called once per frame
    new public void Update()
    {
        if (canMove)
        {
            Vector2 input = new Vector2();
            if (!getOwner().GetComponent<Player>().IsThisPlayer2())
            {
                input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                if(input.x == 0)
                {
                    input = new Vector2(Input.GetAxisRaw("KeyH"), Input.GetAxisRaw("KeyV"));
                }
            }
            else if (getOwner().GetComponent<Player>().IsThisPlayer2())
            {
                input = new Vector2(Input.GetAxisRaw("Horizontal2"), Input.GetAxisRaw("Vertical2"));
                if (input.x == 0)
                {
                    input = new Vector2(Input.GetAxisRaw("KeyH2"), Input.GetAxisRaw("KeyV2"));
                }
            }

            input = input.normalized;

            velocity.x = input.x * moveSpeed;
            velocity.y = input.y * moveSpeed;

            transform.Translate(velocity * Time.deltaTime);
        }

        if (getOwner().GetComponent<Player>().IsThisPlayer2())
        {
            fire = "Fire2";
        }

        if ((Input.GetButtonUp(fire) && !ai) || (ai && inRange))
        {
            owner.GetComponents<AudioSource>()[3].Play();

            /*letGo = true;
            if (!inRange)
            {
                Destroy(this.gameObject);
            }
            else */if (inRange)
            {
                if (hitP1.enabled)
                {
                    if (!hitP1.invincible)
                    {
                        hitP1.ProjectileHit();
                    }
                }
                /*if (hitP2.enabled)
                {
                    if (!hitP2.invincible)
                    {
                        hitP2.ProjectileHit();
                    }
                }*/
            }
            source.setReticleGone();
            if(ai)
                owner.GetComponent<STAI>().endShot();
            Destroy(this.gameObject);
        }

        if (ai && !inRange)
        {
            //move towards player1
            aiTimer--;

            transform.position = Vector3.MoveTowards(transform.position, 
                owner.GetComponent<Player>().opponent.transform.position, 
                moveSpeed * Time.deltaTime);

            if (aiTimer <= 0 && !inRange)
            {
                source.setReticleGone();
                owner.GetComponent<STAI>().endShot();
                Destroy(this.gameObject);
            }
                
        }
    }

    public void SetWeaponSource(STWeapon s)
    {
        source = s;
    }

    new public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(owner))
            {
                inRange = true;

                hitP1 = col.gameObject.GetComponent<Player>();
                //hitP2 = col.gameObject.GetComponent<Player2>();
                /*if (script1.enabled)
                {
                    if (!script1.invincible && letGo)
                    {
                        script1.StunHit(60);
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 26.75f;
                        script1.ProjectileHit();
                        source.setReticleGone();
                        Destroy(this.gameObject);
                    }
                }
                if (script2.enabled)
                {
                    if (!script2.invincible && letGo)
                    {
                        script2.StunHit(60);
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 26.75f;
                        script2.ProjectileHit();
                        source.setReticleGone();
                        Destroy(this.gameObject);
                    }
                }*/
            }
        }
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" && !col.gameObject.Equals(owner))
        {
            inRange = false;
            hitP1 = null;
        }
    }

    public void AIactive(bool b)
    {
        ai = b;
        if (ai)
        {
            aiTimer = 180;
        }
        else
            aiTimer = 0;
    }
}
