﻿using System.Collections.Generic;
using UnityEngine;

public class RWeapon : WeaponScript {

    public Transform deflectPrefab;
    public Player rust;

    public int maxDeflectors = 2;
    public List<GameObject> deflectors = new List<GameObject>();
    public bool shootingStance;
    private float moveSpeed;
    private float newSpeed;
    private float jumpHeight;
    private float newHeight;
    private int stanceCount;

    private int stanceCooldown;


    private Vector2[] directions;
    private int cDirection;

    new public void Start()
    {
        maxBullets = 1;
        shootingStance = false;
        moveSpeed = rust.moveSpeed;
        newSpeed = /*moveSpeed / 4*/ 0.0f;
        jumpHeight = rust.jumpHeight;
        newHeight = /*jumpHeight / 2*/ 0.0f;
        stanceCount = 0;

        stanceCooldown = 0;

        directions = new Vector2[]{ Vector2.right,
            Vector2.up + Vector2.right,
            Vector2.up,
            Vector2.left + Vector2.up,
            Vector2.left
        };

        cDirection = 0;

        base.Start();
    }

    new public void Update()
    {
        if (shootingStance )//&& rust.moveSpeed != newSpeed)
        {
            //rust.StopMovement(60);
            rust.StopActions();
            rust.dashCoolDown = 600;
        }
        
        else if (!shootingStance && stanceCooldown > 0)
        {
            stanceCooldown--;
        }

        base.Update();
    }

    public void Attack(bool isEnemy, bool faceRight, float altFire, float horizontal, float vertical)
    {
        if (CanAttack && altFire != 1 && !shootingStance && stanceCooldown == 0)
        {
            //shootingStance = true;
            shootCooldown = shootingRate;
            var shotTransform = Instantiate(shotPrefab) as Transform;
            shotTransform.position = transform.position;

            RBullet shot = shotTransform.gameObject.GetComponent<RBullet>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            if (shot != null)
            {
                shot.setOwner(gameObject);

                if (faceRight)
                {
                    shot.direction = transform.right; // towards in 2D space is the right of the sprite
                    
                }
                else
                {
                    shot.direction = transform.right * -1;
                }
            }

            bullets.Add(shot);
        }

        else if (CanAttack && altFire != 1 && shootingStance)
        {
            if (rust.isAI)
            {
                horizontal = directions[cDirection].x;
                vertical = directions[cDirection].y;
            }

            //print("Horizontal: " + horizontal);
            //print("Vertical: " + vertical);
            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;

            // The is enemy property
            RBullet shot = shotTransform.gameObject.GetComponent<RBullet>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            if (shot != null)
            {
                shot.setOwner(gameObject);
                shot.HalveSpeed();
                if (faceRight)
                {
                    shot.direction = transform.right; // towards in 2D space is the right of the sprite
                    if (horizontal != 0.0f || vertical != 0.0f)
                    {
                        shot.direction.x = horizontal;
                        shot.direction.y = vertical;
                        if (vertical > 0.0f || vertical < 0.0f)
                        {
                            shot.speed.y = shot.speed.x;
                        }
                        if (horizontal == 0.0f)
                        {
                            shot.speed.x = 0.0f;
                        }
                    }
                }
                else
                {
                    shot.direction = transform.right * -1;
                    if (horizontal != 0.0f || vertical != 0.0f)
                    {
                        shot.direction.x = horizontal;
                        shot.direction.y = vertical;
                        if (vertical > 0.0f || vertical < 0.0f)
                        {
                            shot.speed.y = shot.speed.x;
                        }
                        if (horizontal == 0.0f)
                        {
                            shot.speed.x = 0.0f;
                        }
                    }
                }
            }

            bullets.Add(shot);

            stanceCount++;
            if(stanceCount == 6)
            {
                rust.animate.SetBool("StanceActive", false);
                shootingStance = false;
                stanceCooldown = 60;
                stanceCount = 0;
                maxBullets = 1;
                rust.dashCoolDown = 0;
            }
        }

        if(altFire == 1 && gameObject.GetComponent<Player>().IsGrounded())
        {
            rust.animate.SetBool("StanceActive", true);

            shootCooldown = shootingRate;
            shootingStance = !shootingStance;
            if (!shootingStance)
            {
                maxBullets = 1;
                stanceCooldown = 60;
                stanceCount = 0;
                rust.dashCoolDown = 0;
                rust.animate.SetBool("StanceActive", false);
            }
            else
            {
                rust.dashCoolDown = 600;
                maxBullets = 6;
            }
        }

        /*if (altFire == 1)
        {
            if(deflectors.Count == 2)
            {
                Destroy(deflectors[0]);
                deflectors.Remove(deflectors[0]);
            }

            shootCooldown = shootingRate;

            // Create a new shot
            var deflectorTransform = Instantiate(deflectPrefab) as Transform;

            // Assign position// Assign position
            if (faceRight)
                deflectorTransform.position = new Vector3(transform.position.x + 2.0f, transform.position.y, transform.position.z);
            else
                deflectorTransform.position = new Vector3(transform.position.x - 2.0f, transform.position.y, transform.position.z);


           
            GameObject deflector = deflectorTransform.gameObject;
            
            deflectors.Add(deflector);
        }*/

    }

    public void SetCurrentDirection(int d)
    {
        cDirection = d;
    }
    public void IncrementDirection()
    {
        cDirection++;
        if(cDirection >= directions.Length)
        {
            cDirection = 0;
        }
    }
    public void DecrementDirection()
    {
        cDirection--;
        if (cDirection < 0)
        {
            cDirection = directions.Length-1;
        }
    }

    public void EndStance()
    {
        shootingStance = false;
        maxBullets = 1;
        stanceCooldown = 60;
        stanceCount = 0;
        rust.dashCoolDown = 0;
        rust.animate.SetBool("StanceActive", false);
        if (rust.isAI)
        {
            GetComponent<RAI>().EndStance();
        }
    }
}
