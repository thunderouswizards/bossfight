﻿using UnityEngine;
using System.Collections;

public class RBullet : BulletScript {

    private int timeToLive = 120;

    new public void Update()
    {
        // 2 - Movement
        /* movement = new Vector2(
             speed.x * direction.x,
             speed.y * direction.y);*/


        movement = new Vector2(
            speed.x * direction.x,
            speed.y * direction.y);

        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if ((screenPosition.x > Screen.width || screenPosition.x < 0) ||
            (screenPosition.y > Screen.height || screenPosition.y < 0))
            Destroy(this.gameObject);

        if(timeToLive <= 0)
        {
            Destroy(this.gameObject);
        }

        timeToLive--;
    }

    new public void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = movement;
        float zAngle = Mathf.Atan2(GetComponent<Rigidbody2D>().velocity.y, GetComponent<Rigidbody2D>().velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, zAngle);
    }

    new public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(owner))
            {
                Player script1 = col.gameObject.GetComponent<Player>();
                //Player2 script2 = col.gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible)
                    {
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 13.75f;
                        script1.ProjectileHit();
                        Destroy(this.gameObject);
                    }
                }
                /*if (script2.enabled)
                {
                    if (!script2.invincible)
                    {
                        Vector3 dir = (col.transform.position - transform.position).normalized;
                        dir.y = 1;
                        col.attachedRigidbody.velocity = dir * 13.75f;
                        script2.ProjectileHit();
                        Destroy(this.gameObject);
                    }
                }*/
            }
        }

        else if (col.gameObject.tag == "Wall")
        {
            /*direction.x = -1 * direction.x;
            direction.y = 1.0f;
            speed.y = speed.x;*/
            Destroy(this.gameObject);
        }

        /*else if (col.gameObject.tag == "Deflector")
        {
            direction.y = 1.0f;
            direction.x = 0.0f;
            speed.y = speed.x;
            speed.x = 0;
        }*/
    }

    public void HalveSpeed()
    {
        speed.x /= 4.0f;
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Deflector")
        {
            Destroy(col.gameObject);
        }
    }
}
