﻿using UnityEngine;
using System.Collections;

public class TKSpear : BulletScript {

    Vector3 nextPos;
    private float v0 = 20.0f;
    private float startAngle = 70.0f;
    private float v0x, v0y;
    private int startTime;
    private int timeMark;
    float startX;
    float startY;
    bool goingUp = false;
    bool peak = false;
    bool rotated = false;

    new public void Start()
    {
        v0x = v0 * (float)(System.Math.Cos(startAngle));
        v0y = v0 * (float)(System.Math.Sin(startAngle));
        startTime = 0;
        timeMark = 1;
        startX = transform.position.x;
        startY = transform.position.y;
    }

    new public void Update()
    {
        if (direction.x > 0 && !rotated)
        {
            this.transform.rotation = new Quaternion(this.transform.rotation.x, 180, this.transform.rotation.z, this.transform.rotation.w);
            rotated = true;
        }

        if (startTime <= 0 )
        {
            float nextX = v0x * timeMark;
            if (direction.x > 0.0f)
                nextX = startX + nextX;
            else
                nextX = startX - nextX;
            float nextY = startY + v0y * timeMark - 9.8f * (timeMark * timeMark);
            nextPos = new Vector3(nextX, nextY, transform.position.z);
            //Rotate here. Point towards nextPos
            float zAngle = Mathf.Atan2(transform.position.y - nextY, transform.position.x - nextX) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, zAngle);

            startTime = 30;
            timeMark++;
        }

        transform.position = Vector3.MoveTowards(transform.position, nextPos, v0 * Time.deltaTime);
        startTime--;

        
    }

    new public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(owner))
            {
                Player script1 = col.gameObject.GetComponent<Player>();
                //Player2 script2 = col.gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible)
                    {
                        script1.StunHit(120);
                    }
                }
                /*if (script2.enabled)
                {
                    if (!script2.invincible)
                    {
                        script2.StunHit(120);
                    }
                }*/
            }
        }
        else if (col.gameObject.tag == "Wall" || col.gameObject.tag == "Floor")
        {
            Destroy(this.gameObject);
        }
    }
}
