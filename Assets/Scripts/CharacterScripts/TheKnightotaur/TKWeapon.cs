﻿using UnityEngine;
using System.Collections;

public class TKWeapon : WeaponScript {

    public Transform spearPrefab;
    public TKSpear currentSpear;
    
    // Use this for initialization
    new public void Start () {
        maxBullets = 1;
        base.Start();
	}

    new public void Attack(bool isEnemy, bool faceRight, float altFire)
    {

        if (CanAttack && altFire != 1)
        {
            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;

            // The is enemy property
            TKBullet shot = shotTransform.gameObject.GetComponent<TKBullet>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            if (shot != null)
            {
                shot.setOwner(gameObject);

                if (faceRight)
                    shot.direction = transform.right; // towards in 2D space is the right of the sprite
                else
                    shot.direction = transform.right * -1;
            }

            bullets.Add(shot);
        }

        if (altFire == 1 && currentSpear == null)
        {
            shootCooldown = shootingRate;

            // Create a new shot
            var spearTransform = Instantiate(spearPrefab) as Transform;

            // Assign position
            spearTransform.position = transform.position;

            // The is enemy property
            TKSpear spear = spearTransform.gameObject.GetComponent<TKSpear>();
            if (spear != null)
            {
                spear.isEnemyShot = isEnemy;
            }

            if (spear != null)
            {
                spear.setOwner(gameObject);

                if (faceRight)
                    spear.direction = transform.right; // towards in 2D space is the right of the sprite
                else
                    spear.direction = transform.right * -1;
            }

            currentSpear = spear;
        }

    }
}
