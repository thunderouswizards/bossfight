﻿using UnityEngine;

/// <summary>
/// Projectile behavior
/// </summary>
public class BulletScript : MonoBehaviour
{
	// 1 - Designer variables
	
	/// <summary>
	/// Damage inflicted
	/// </summary>
	public int damage = 1;
	private bool altFire = true;
	private float incrementAlt = 7f;
	public Vector2 speed = new Vector2(20.0f, 0.0f);
	public GameObject owner;


	public Vector2 direction = new Vector2(-1.0f, 0.0f);
	
	public Vector2 movement;
	/// <summary>
	/// Projectile damage player or enemies?
	/// </summary>
	public bool isEnemyShot = false;

	public void Start()
	{
		// 2 - Limited time to live to avoid any leak
		//Destroy(gameObject, 5); // 5sec
	}

	public void Update()
	{
		if (altFire && incrementAlt <= 0f) {
			//direction.y = 1;
			speed.y *= 2;
			/*if(speed.y > speed.x){
				speed.y = speed.x;
			}*/
			incrementAlt = 7f;
		}

		// 2 - Movement
		movement = new Vector2(
			speed.x * direction.x,
			speed.y * direction.y);


        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		if ((screenPosition.x > Screen.width || screenPosition.x < 0) || 
		    (screenPosition.y > Screen.height || screenPosition.y < 0))
			Destroy(this.gameObject);

		incrementAlt--;
	}
	
	public void FixedUpdate()
	{
		// Apply movement to the rigidbody
		GetComponent<Rigidbody2D>().velocity = movement;
	}

	public void setAltFire(){
		speed.y = 1;
		direction.y = 1;
		altFire = true;
	}

    public void setAltFireAirborne()
    {
        speed.y = 1;
        direction.y = -1;
        altFire = true;
    }

	public void setOwner(GameObject player){
		owner = player;
	}

	public GameObject getOwner(){
		return owner;
	}

	public void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			if(!col.gameObject.Equals(owner)){
				Player script1 = col.gameObject.GetComponent<Player>();
				//Player2 script2 = col.gameObject.GetComponent<Player2>();
				if(script1.enabled){
					if(!script1.invincible){
						Vector3 dir = (col.transform.position - transform.position).normalized;
						dir.y = 1;
						col.attachedRigidbody.velocity = dir * 13.75f;
						script1.ProjectileHit();
						Destroy (this.gameObject);
					}
				}
				/*if(script2.enabled){
					if(!script2.invincible){
						Vector3 dir = (col.transform.position - transform.position).normalized;
						dir.y = 1;
						col.attachedRigidbody.velocity = dir * 13.75f;
						script2.ProjectileHit();
						Destroy (this.gameObject);
					}
				}*/
			}
		}
        else if (col.gameObject.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }
}
