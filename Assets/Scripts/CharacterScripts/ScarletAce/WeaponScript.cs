﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Launch projectile
/// </summary>
public class WeaponScript : MonoBehaviour
{
	//--------------------------------
	// 1 - Designer variables
	//--------------------------------
	
	/// <summary>
	/// Projectile prefab for shooting
	/// </summary>
	public Transform shotPrefab;
	
	/// <summary>
	/// Cooldown in seconds between two shots
	/// </summary>
	public float shootingRate = 120f;
    public float altRate = 120f;

    public int maxBullets = 3;

	public List<BulletScript> bullets = new List<BulletScript>();
	//--------------------------------
	// 2 - Cooldown
	//--------------------------------
	
	public float shootCooldown;
    public float altCooldown;
	
	public void Start()
	{
		shootCooldown = 0f;
        altCooldown = 0f;
	}
	
	public void Update()
	{
		if (shootCooldown > 0)
		{
			shootCooldown -= 1;
		}

        if (altCooldown > 0)
        {
            altCooldown -= 1;
        }

        for (int i = 0; i < bullets.Count; i++)
		{
			if(bullets[i] == null)
				bullets.Remove (bullets[i]);
		}
		//print ("Bullets: " + bullets.Count);
	}
	
	//--------------------------------
	// 3 - Shooting from another script
	//--------------------------------
	
	/// <summary>
	/// Create a new projectile if possible
	/// </summary>
	public void Attack(bool isEnemy, bool faceRight, float altFire)
	{
		if (CanAttack)
		{
			shootCooldown = shootingRate;
			
			// Create a new shot
			var shotTransform = Instantiate(shotPrefab) as Transform;
			
			// Assign position
			shotTransform.position = transform.position;
			
			// The is enemy property
			BulletScript shot = shotTransform.gameObject.GetComponent<BulletScript>();
			if (shot != null)
			{
				shot.isEnemyShot = isEnemy;
			}

			if (shot != null)
			{
				shot.setOwner (this.gameObject);

				if(faceRight)
					shot.direction = this.transform.right; // towards in 2D space is the right of the sprite
				else
					shot.direction = this.transform.right * -1;

				if(altFire == 1 && gameObject.GetComponent<Player>().IsGrounded()){
					shot.setAltFire ();
				}
                else if (altFire == 1 && !gameObject.GetComponent<Player>().IsGrounded())
                {
                    shot.setAltFireAirborne();
                }
			}

			bullets.Add(shot);
		}
	}
	
	/// <summary>
	/// Is the weapon ready to create a new projectile?
	/// </summary>
	public bool CanAttack
	{
		get
		{
			return shootCooldown <= 0f && bullets.Count < maxBullets;
		}
	}
    public bool CanAlt
    {
        get
        {
            return altCooldown <= 0f;
        }
    }
}