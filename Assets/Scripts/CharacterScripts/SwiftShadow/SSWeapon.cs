﻿using UnityEngine;
using System.Collections;

public class SSWeapon : WeaponScript {

    private Player swiftShadow;

    private string horizontalKey = "Horizontal";
    private string keyHKey = "KeyH";
    private string keyVKey = "KeyV";
    private string verticalKey = "Vertical";
    /*private string fireKey = "Fire";
    private string altKey = "AltFire";
    private string jumpKey = "Jump";
    private string dashKey = "Dash";*/

    //Changing alt-fire from going invisible to throwing out a log that SS can swap with up to three times.

    //public GameObject SSModel;
    //private int invisCount;

    public GameObject Log;
    private GameObject currentLog;
    //private int teleportCount;

    private int teleportWindow;
    private float tdX;

    // Use this for initialization
    new public void Start () {
        maxBullets = 5;
        teleportWindow = 60;
        //invisCount = -1;

        swiftShadow = gameObject.GetComponent<Player>();

        if (swiftShadow.IsThisPlayer2())
        {
            horizontalKey = "Horizontal2";
            keyHKey = "KeyH2";
            verticalKey = "Vertical2";
            keyVKey = "KeyV2";
            /*fireKey = "Fire2";
            altKey = "AltFire2";
            jumpKey = "Jump2";
            dashKey = "Dash2";*/
        }
        

        base.Start();
    }

    new public void Update()
    {
        /*if(invisCount > 0)
        {
            invisCount--;
        }

        if(invisCount == 0)
        {
            SSModel.SetActive(true);
            invisCount--;
        }*/

        if(teleportWindow != 60)
        {
            teleportWindow++;
        }

        base.Update();
    }

    new public void Attack(bool isEnemy, bool faceRight, float altFire)
    {

        if (CanAttack && altFire != 1)
        {
            shootCooldown = shootingRate;

            // Create a new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // Assign position
            shotTransform.position = transform.position;

            // The is enemy property
            SSBullet shot = shotTransform.gameObject.GetComponent<SSBullet>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            if (shot != null)
            {
                shot.setOwner(gameObject);

                if (faceRight)
                    shot.direction = transform.right; // towards in 2D space is the right of the sprite
                else
                    shot.direction = transform.right * -1;
            }

            bullets.Add(shot);
        }

        if (altFire == 1 /*&& currentLog == null*/)
        {
            shootCooldown = teleportWindow;

            tdX = Input.GetAxisRaw(horizontalKey);
            if(tdX == 0.0f)
                tdX = Input.GetAxisRaw(keyHKey);

            teleportWindow = 0;
            swiftShadow.StopMovement(60);
            swiftShadow.invinCount = 60;
            swiftShadow.invincible = true;

            /*currentLog = Instantiate(Log) as GameObject;
            currentLog.transform.position = transform.position;
            SSLog l = currentLog.GetComponent<SSLog>();
            if(l != null)
            {
                if (faceRight)
                    l.direction = transform.right;
                else
                    l.direction = transform.right * -1;

                l.ThrowLog();
            }*/


            //SSModel.SetActive(false);
            //invisCount = 90;

            /*shootCooldown = shootingRate;
            Vector2 input = new Vector2(Input.GetAxisRaw(horizontalKey), Input.GetAxisRaw(verticalKey));
            if (input.x == 0 && input.y == 0)
            {
                input = new Vector2(Input.GetAxisRaw(keyHKey), Input.GetAxisRaw(keyVKey));
            }
            float newX = transform.position.x;
            float newY = transform.position.y;
            if (input.x > 0)
                newX += 5.0f;
            else if (input.x < 0)
                newX -= 5.0f;
            
            if (input.y > 0)
                newY += 5.0f;
            else if (input.y < 0)
                newY -= 5.0f;

            Vector3 newPos = new Vector3(newX, newY, transform.position.z);
            transform.position = newPos;*/
            //gameObject.transform.position.Set(gameObject.transform.position.x + 5.0f,gameObject.transform.position.y,gameObject.transform.position.z);   
        }

        /*else if (altFire == 1 && currentLog != null)
        {
            shootCooldown = shootingRate;

            Vector3 currentPos = transform.position;
            transform.position = currentLog.transform.position;
            currentLog.transform.position = currentPos;
            currentLog.GetComponent<SSLog>().Stop();

            teleportCount++;
            if(teleportCount == 3)
            {
                teleportCount = 0;
                Destroy(currentLog);
                //currentLog = null;
            }

        }*/

    }

    public void Teleport()
    {
        Vector3 tPos;
        float tX;
        float tY = transform.position.y + 5.0f;

        if (tdX > 0.0)
            tX = transform.position.x + 5.0f;
        else if (tdX < 0.0)
            tX = transform.position.x - 5.0f;
        else
            tX = transform.position.x;

        tPos = new Vector2(tX, tY);
        transform.position = tPos;

        swiftShadow.StopMovement(0);
        swiftShadow.invinCount = 0;
        swiftShadow.invincible = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Projectile" && teleportWindow < 60)
        {
            Teleport();
            Destroy(collision.gameObject);
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Player>() != null)
        {
            if (collision.gameObject.tag == "Player" && teleportWindow < 60 && collision.gameObject.GetComponent<Player>().getIsDashing())
            {
                Teleport();
            }
        }

    }
}
