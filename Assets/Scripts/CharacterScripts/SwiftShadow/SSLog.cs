﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSLog : MonoBehaviour {

    public Vector2 direction;
    private Rigidbody2D rb;

    // Use this for initialization
    void Start () {
        direction = new Vector2(-1.0f, 1.0f);
        rb = gameObject.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    void FixedUpdate()
    {

    }

    public void ThrowLog()
    {

        Vector2 launch = new Vector3(direction.x * 5.0f, 15.0f);
        gameObject.GetComponent<Rigidbody2D>().velocity = launch;
    }

    public void Stop()
    {
        rb.velocity = new Vector2(0.0f, rb.velocity.y);
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player" && gameObject.GetComponent<BoxCollider2D>().isTrigger)
        {
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }
}
