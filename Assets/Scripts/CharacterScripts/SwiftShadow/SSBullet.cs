﻿using UnityEngine;
using System.Collections;

public class SSBullet : BulletScript {

    private bool rotated;


    new public void Update()
    {
        if (direction.x > 0 && !rotated)
        {
            this.transform.rotation = new Quaternion(this.transform.rotation.x, 180, this.transform.rotation.z, this.transform.rotation.w);
            rotated = true;
        }
        base.Update();
    }

    new public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!col.gameObject.Equals(owner))
            {
                Player script1 = col.gameObject.GetComponent<Player>();
                //Player2 script2 = col.gameObject.GetComponent<Player2>();
                if (script1.enabled)
                {
                    if (!script1.invincible)
                    {
                        script1.StunHit(120);
                        Destroy(this.gameObject);
                    }
                }
                /*if (script2.enabled)
                {
                    if (!script2.invincible)
                    {
                        script2.StunHit(120);
                        Destroy(this.gameObject);
                    }
                }*/
            }
        }
        else if (col.gameObject.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }
}
