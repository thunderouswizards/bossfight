﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSelect : MonoBehaviour {

    public ArcadeManager aManager;

    public Animator menu;

    public MeshRenderer channelUp, channelDown, ir;

    private string horizontal = "Horizontal";
    private string vertical = "Vertical";
    private string keyH = "KeyH";
    private string keyV = "KeyV";
    private string fire = "Fire";
    private string jump = "Jump";

    private int current, lastInput, inputTimer;

    public GameObject controlsBack;
    public GameObject controlsText;
    private string active, growing, inactive, shrinking;
    private string controlsState;

    private bool loadScene;
    private string characterSelect;

    // Use this for initialization
    void Start () {
        current = 0;
        lastInput = 0;
        menu.Play("ChangeToMenu", -1);
        inputTimer = 31;

        active = "ACTIVE";
        growing = "GROWING";
        inactive = "INACTIVE";
        controlsState = inactive;

        loadScene = false;
	}

    // Update is called once per frame
    void Update()
    {
        if (loadScene)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(0.0f,0.0f, -5.0f), 50.0f * Time.deltaTime);
            if(Camera.main.transform.position.z >= -5.0f)
            {
                menu.Play("Scramble", -1);
                SceneManager.LoadScene(characterSelect);
            }
        }

        else
        {


            if (controlsState == growing)
            {
                Vector3 s = controlsBack.transform.localScale;
                controlsBack.transform.localScale = new Vector3(s.x + 0.1f, s.y + 0.1f, s.z + 0.1f);
                if (controlsBack.transform.localScale.x >= 1.0f)
                {
                    controlsState = active;
                    controlsText.SetActive(true);
                }

            }
            else if (controlsState == active)
            {
                if (Input.GetButtonUp(jump))
                {
                    //controlsBack.SetActive(false);
                    controlsText.SetActive(false);
                    controlsState = shrinking;
                }
            }
            else if (controlsState == shrinking)
            {
                Vector3 s = controlsBack.transform.localScale;
                controlsBack.transform.localScale = new Vector3(s.x - 0.1f, s.y - 0.1f, s.z - 0.1f);
                if (controlsBack.transform.localScale.x <= 0.1f)
                {
                    controlsState = inactive;
                    controlsText.SetActive(false);
                }
            }
            else
            {
                if (inputTimer == 31)
                {
                    float input = Input.GetAxis(vertical);
                    if (input == 0.0f)
                    {
                        input = Input.GetAxis(keyV);
                    }

                    if (input == 0.0f)
                    {
                        channelUp.material.DisableKeyword("_EMISSION");
                        channelDown.material.DisableKeyword("_EMISSION");
                        ir.material.DisableKeyword("_EMISSION");
                    }

                    if (input > 0.0f /*&& lastInput != 1*/)
                    {
                        channelUp.material.EnableKeyword("_EMISSION");
                        ir.material.EnableKeyword("_EMISSION");

                        current += 1;
                        if (current >= 4)
                            current = 0;
                        //lastInput = 1;

                        inputTimer--;

                    }
                    else if (input < 0.0f /*&& lastInput != -1*/)
                    {
                        channelDown.material.EnableKeyword("_EMISSION");
                        ir.material.EnableKeyword("_EMISSION");

                        current -= 1;
                        if (current <= -1)
                            current = 3;
                        //lastInput = -1;

                        inputTimer--;
                    }

                    switch (current)
                    {
                        case 0:
                            menu.Play("ChangeToMenu", -1);
                            break;
                        case 1:
                            menu.Play("ChangeToVS", -1);
                            break;
                        case 2:
                            menu.Play("ChangeToArcade", -1);
                            break;
                        case 3:
                            menu.Play("ChangeToControls", -1);
                            break;
                    }


                    if (Input.GetButtonUp(jump))
                    {
                        switch (current)
                        {
                            case 0:
                                SceneManager.LoadScene("TitleScreen");
                                break;
                            case 1:
                                loadScene = true;
                                characterSelect = "CharacterSelect";
                                break;
                            case 2:
                                aManager.selectArcade();
                                loadScene = true;
                                characterSelect = "1PCharacterSelect";
                                break;
                            case 3:
                                controlsBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                                controlsBack.SetActive(true);
                                //controlsText.SetActive(true);
                                controlsState = growing;
                                break;
                        }
                    }

                    if (Input.GetKeyDown("escape"))
                    {
                        Application.Quit();
                    }
                }

                else
                {
                    inputTimer--;
                    if (inputTimer == 0)
                        inputTimer = 31;
                }
            }
        }
    }
}
