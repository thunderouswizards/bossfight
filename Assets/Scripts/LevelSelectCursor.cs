﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectCursor : MonoBehaviour {

    public FadeToBlack ftb;
    public CanvasScaler scaler;

    Vector3 velocity;
	float velocityXSmoothing;
	int moveSpeed = 400;
	bool canMove;
	int levelSelected;
	public RectTransform[] selectSquares;
    public RectTransform pointer;

    private AudioSource sound;

    public GameObject[] Displays;

    // Use this for initialization
    void Start () {
        moveSpeed = (int)((moveSpeed / 1024.0f) * Screen.width);

        levelSelected = -1;
		canMove = true;

        sound = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
		CheckInCharacter ();
		
		if (canMove) {
			Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
            if(input.x == 0 && input.y == 0)
            {
                input = new Vector2(Input.GetAxisRaw("KeyH"), Input.GetAxisRaw("KeyV"));
            }
			
			velocity.x = input.x * moveSpeed;
			velocity.y = input.y * moveSpeed;
			
			transform.Translate (velocity * Time.deltaTime);
            if(transform.position.x + (pointer.rect.width/2.0f) > Screen.width)
            {
                transform.position = new Vector3(Screen.width - (pointer.rect.width / 2.0f), transform.position.y);
            }
            else if (transform.position.x - (pointer.rect.width / 2.0f) < 0.0f)
            {
                transform.position = new Vector3(pointer.rect.width / 2.0f, transform.position.y);
            }

            if (transform.position.y + (pointer.rect.height / 2.0f) > Screen.height)
            {
                transform.position = new Vector3(transform.position.x, Screen.height - (pointer.rect.height / 2.0f));
            }
            else if (transform.position.y - (pointer.rect.height / 2.0f) < 0.0f)
            {
                transform.position = new Vector3(transform.position.x, pointer.rect.height / 2.0f);
            }
        }
		
		if (Input.GetButtonUp ("Jump") && levelSelected != -1) {
            sound.Play();

            /*if(levelSelected == 0)
			    SceneManager.LoadScene("project");
            else if(levelSelected == 1)
                SceneManager.LoadScene("SmallStage");
            else if (levelSelected == 2)
                SceneManager.LoadScene("DestrukTorrLevel");
            else if (levelSelected == 3)
                SceneManager.LoadScene("RustLevel");
            else if (levelSelected == 4)
                SceneManager.LoadScene("KingMaximumLevel");
            else if (levelSelected == 5)
                SceneManager.LoadScene("ST02Level");
            else if (levelSelected == 6)
                SceneManager.LoadScene("TheKnightotaurLevel");
            else if (levelSelected == 7)
                SceneManager.LoadScene("SwiftShadowLevel");
            else if(levelSelected == 8)
            {
                Destroy(GameObject.FindGameObjectWithTag("LoaderInfo"));
                SceneManager.LoadScene("CharacterSelect");
            }*/

            if (levelSelected == 0)
                ftb.StartFading("SALevel");
            else if (levelSelected == 1)
                ftb.StartFading("MSLevel");
            else if (levelSelected == 2)
                ftb.StartFading("DestrukTorrLevel");
            else if (levelSelected == 3)
                ftb.StartFading("RustLevel");
            else if (levelSelected == 4)
                ftb.StartFading("KingMaximumLevel");
            else if (levelSelected == 5)
                ftb.StartFading("ST02Level");
            else if (levelSelected == 6)
                ftb.StartFading("TheKnightotaurLevel");
            else if (levelSelected == 7)
                ftb.StartFading("SwiftShadowLevel");
            else if (levelSelected == 8)
            {
                Destroy(GameObject.FindGameObjectWithTag("LoaderInfo"));
                SceneManager.LoadScene("CharacterSelect");
            }

            if (levelSelected >= 0 && levelSelected <= 7)
                this.enabled = false;

        }

        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }
    }
	
	void CheckInCharacter(){
		float width, height, left, right, top, bottom;
        float selectX, selectY;

        float pointerW = pointer.rect.width;
        pointerW = (pointerW / scaler.referenceResolution.x) * Screen.width;

        float pointerH = pointer.rect.height;
        pointerH = (pointerH / scaler.referenceResolution.y) * Screen.height;

        selectX = (transform.localPosition.x / scaler.referenceResolution.x) * Screen.width + pointerW / 2.0f;
        selectY = (transform.localPosition.y / scaler.referenceResolution.y) * Screen.height + pointerH / 2.0f;

        for (int i = 0; i < selectSquares.Length; i++) {
			width = selectSquares[i].rect.width;
			height = selectSquares[i].rect.height;

            width = (width / scaler.referenceResolution.x) * Screen.width;
            height = (height / scaler.referenceResolution.y) * Screen.height;

            float xPos = selectSquares[i].localPosition.x / scaler.referenceResolution.x * Screen.width;
            float yPos = selectSquares[i].localPosition.y / scaler.referenceResolution.y * Screen.height;

            left = xPos - width / 2.0f;
            right = xPos + width / 2.0f;
            top = yPos + height / 2.0f;
            bottom = yPos - height / 2.0f;

            if ((selectX > left && selectX < right) && (selectY < top && selectY > bottom)){
                if(levelSelected != -1 && levelSelected != Displays.Length)
                {
                    Displays[levelSelected].SetActive(false);
                }

                levelSelected = i;
                if(i < Displays.Length && i >= 0)
                    Displays[i].SetActive(true);
			}
            else if (selectX < left || selectX > right || selectY > top || selectY < bottom)
            {
                if(levelSelected == i && levelSelected != Displays.Length)
                {
                    Displays[i].SetActive(false);
                    levelSelected = -1;
                }
                    
            }
		}
	}
}
