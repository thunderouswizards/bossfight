﻿using UnityEngine;
using System.Collections;

public class CharacterSelectManager : MonoBehaviour {

    public bool singleMode;

	private int playerOne;
	private int playerTwo;
	public GameObject readyToStart;

	// Use this for initialization
	void Start () {
		playerOne = -1;
		playerTwo = -1;
	}
	
	// Update is called once per frame
	void Update () {

        if (singleMode)
        {
            if (playerOne != -1)
            {
                readyToStart.SetActive(true);
            }
            if (playerOne == -1)
            {
                readyToStart.SetActive(false);
            }
        }

        else
        {
            if (playerOne != -1 && playerTwo != -1)
            {
                readyToStart.SetActive(true);
            }

            if (playerOne == -1 || playerTwo == -1)
            {
                readyToStart.SetActive(false);
            }
        }

	}

	public void SetPlayerOne(int value){
		playerOne = value;
	}

	public void SetPlayerTwo(int value){
        playerTwo = value;
	}
}
