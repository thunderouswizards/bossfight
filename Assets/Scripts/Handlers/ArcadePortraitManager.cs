﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcadePortraitManager : MonoBehaviour {

    public Animator screen;
    public GameObject[] player;
    public GameObject[] enemy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisplayPortraits(string p, string e)
    {
        int pIndex = 0, eIndex = 0;

        switch (p)
        {
            case "ScarletAce":
                pIndex = 0;
                break;
            case "MysticSapphire":
                pIndex = 1;
                break;
            case "DestrukTorr":
                pIndex = 2;
                break;
            case "KingMaximum":
                pIndex = 3;
                break;
            case "Rust":
                pIndex = 4;
                break;
            case "TheKnightotaur":
                pIndex = 5;
                break;
            case "ST-02":
                pIndex = 6;
                break;
            case "SwiftShadow":
                pIndex = 7;
                break;
            default:
                pIndex = 0;
                break;
        }

        switch (e)
        {
            case "ScarletAce":
                eIndex = 0;
                break;
            case "MysticSapphire":
                eIndex = 1;
                break;
            case "DestrukTorr":
                eIndex = 2;
                break;
            case "KingMaximum":
                eIndex = 3;
                break;
            case "Rust":
                eIndex = 4;
                break;
            case "TheKnightotaur":
                eIndex = 5;
                break;
            case "ST-02":
                eIndex = 6;
                break;
            case "SwiftShadow":
                eIndex = 7;
                break;
            default:
                eIndex = 0;
                break;
        }

        player[pIndex].SetActive(true);
        enemy[eIndex].SetActive(true);
    }

    public void DisplayWinScreen()
    {
        screen.Play("ChangeToWin", -1);
    }
}
