﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SelectManager : MonoBehaviour {

    public bool ArcadeMode;

    public GameObject[] P2Stuff;
    public BlinkingText bt;
    public CharacterSelectManager csm;

    private string level;

    private string playerOne;
    private string playerTwo;
    private int p1v;
    private int p2v;
    private int playerThatWon;
    private bool playersLoaded;

    private SetVictoryScreen svs;

    private CharacterLoader loader;

	// Use this for initialization
	void Start () {
        playerOne = "ScarletAce";
        playerTwo = "ScarletAce";
        p1v = 0;
        p2v = 0;
        playerThatWon = 0;
        playersLoaded = false;
        DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        if (SceneManager.GetActiveScene().name != "CharacterSelect"
            && SceneManager.GetActiveScene().name != "LevelSelect"
            && SceneManager.GetActiveScene().name != "ArcadeMode"
            && SceneManager.GetActiveScene().name != "1PCharacterSelect"
            && SceneManager.GetActiveScene().name != "MainMenu"
            && SceneManager.GetActiveScene().name != "NewMainMenu"
            && SceneManager.GetActiveScene().name != "VictoryScreen"
            && !playersLoaded)
        {
            loader = GameObject.FindGameObjectWithTag("CharacterLoader").GetComponent<CharacterLoader>();
            loader.LoadCharacters(playerOne, playerTwo, ArcadeMode);
            //LoadCharacters();
            playersLoaded = true;
            if (ArcadeMode)
                level = SceneManager.GetActiveScene().name;
        }

        /*else if((Application.loadedLevelName != "CharacterSelect" || Application.loadedLevelName != "LevelSelect") && playersLoaded)
        {
            playersLoaded = false;
        }*/

        if (SceneManager.GetActiveScene().name == "VictoryScreen")
        {
            if (svs == null)
            {
                svs = GameObject.FindGameObjectWithTag("VictoryLoader").GetComponent<SetVictoryScreen>();
                svs.SetPlayerWon(playerThatWon);
                if (playerThatWon == 0)
                    svs.SetPortrait(p1v);
                else
                    svs.SetPortrait(p2v);
            }

            if (Input.GetButtonUp("Start"))
            {
                if (ArcadeMode)
                {
                    playersLoaded = false;

                    string scene = "ArcadeMode";

                    string winner = GameObject.FindGameObjectWithTag("WinnerPortrait").name;
                    if (winner.Equals(playerOne))
                    {
                        scene = "ArcadeMode";
                    }
                    else
                        scene = level;
                    GameObject.FindGameObjectWithTag("BlackFade").GetComponent<FadeToBlack>().StartFading(scene);
                }
                else
                {
                    GameObject.FindGameObjectWithTag("BlackFade").GetComponent<FadeToBlack>().StartFading("CharacterSelect");
                    Destroy(this.gameObject);
                }
            }
                
        }

        if (SceneManager.GetActiveScene().name == "MainMenu" || SceneManager.GetActiveScene().name == "NewMainMenu")
            Destroy(this.gameObject);
	}

    public void SetPlayerOne(int charIndex)
    {
        p1v = charIndex;

        switch (charIndex)
        {
            case 0:
                playerOne = "ScarletAce";
                break;
            case 1:
                playerOne = "MysticSapphire";
                break;
			case 2:
				playerOne = "DestrukTorr";
				break;
			case 3:
				playerOne = "KingMaximum";
				break;
            case 4:
                playerOne = "Rust";
                break;
            case 5:
                playerOne = "TheKnightotaur";
                break;
            case 6:
                playerOne = "ST-02";
                break;
            case 7:
                playerOne = "SwiftShadow";
                break;
            default:
                playerOne = "ScarletAce";
                break;
        }
    }

    public void SetPlayerTwo(int charIndex)
    {
        p2v = charIndex;

        switch (charIndex)
        {
            case 0:
                playerTwo = "ScarletAce";
                break;
            case 1:
                playerTwo = "MysticSapphire";
                break;
            case 2:
                playerTwo = "DestrukTorr";
                break;
            case 3:
                playerTwo = "KingMaximum";
                break;
            case 4:
                playerTwo = "Rust";
                break;
            case 5:
                playerTwo = "TheKnightotaur";
                break;
            case 6:
                playerTwo = "ST-02";
                break;
            case 7:
                playerTwo = "SwiftShadow";
                break;
            default:
                playerTwo = "ScarletAce";
                break;
        }

        /*switch (charIndex)
        {
            case 0:
                playerTwo = "ScarletAce2";
                break;
            case 1:
                playerTwo = "MysticSapphire2";
                break;
			case 2:
				playerTwo = "DestrukTorr2";
				break;
            case 3:
                playerTwo = "KingMaximum2";
                break;
            case 4:
                playerTwo = "Rust2";
                break;
            case 5:
                playerTwo = "TheKnightotaur2";
                break;
            case 6:
                playerTwo = "ST-022";
                break;
            case 7:
                playerTwo = "SwiftShadow2";
                break;
            default:
                playerTwo = "ScarletAce2";
                break;
        }*/
    }

    /*public void LoadCharacters()
    {
        GameObject[] playersPicked = GameObject.FindGameObjectsWithTag("Player");
        int p1Index = 0, p2Index = 0;
        for (int i = 0; i < playersPicked.Length; i++)
        {
            if (!playersPicked[i].name.Equals(playerOne) && !playersPicked[i].name.Equals(playerTwo))
            {
                playersPicked[i].SetActive(false);
            }

            if (playersPicked[i].name.Equals(playerOne))
            {
                p1Index = i;
            }

            if (playersPicked[i].name.Equals(playerTwo))
            {
                p2Index = i;
            }
        }

        playersPicked[p1Index].GetComponent<Player>().SetOpponent(playersPicked[p2Index].GetComponent<Player>());
        playersPicked[p2Index].GetComponent<Player>().SetOpponent(playersPicked[p1Index].GetComponent<Player>());
    }*/

    public void SetPlayerTwo(string opponent)
    {
        playerTwo = opponent;

        switch (playerTwo)
        {
            case "ScarletAce":
                p2v = 0;
                break;
            case "MysticSapphire":
                p2v = 1;
                break;
            case "DestrukTorr":
                p2v = 2;
                break;
            case "KingMaximum":
                p2v = 3;
                break;
            case "Rust":
                p2v = 4;
                break;
            case "TheKnightotaur":
                p2v = 5;
                break;
            case "ST-02":
                p2v = 6;
                break;
            case "SwiftShadow":
                p2v = 7;
                break;
            default:
                p2v = 0;
                break;
        }

    }

    public void SetPlayerWon(int num)
    {
        playerThatWon = num;
    }

    public bool CheckLevelScene()
    {
        return false;
    }

    public string GetPlayerOne()
    {
        return playerOne;
    }

    public void SetArcade()
    {
        ArcadeMode = true;
        for(int i = 0; i < P2Stuff.Length; i++)
        {
            P2Stuff[i].SetActive(false);
        }
        bt.sceneName = "ArcadeMode";
        csm.singleMode = true;
    }
}
