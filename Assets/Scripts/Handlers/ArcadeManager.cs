﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ArcadeManager : MonoBehaviour {

    private int matchesWon;
    private bool inArcade, nextFightSet, returnToMain;

    private List<ArcadeLevel> levels;

    public struct ArcadeLevel
    {
        public string level;
        public string character;

        public ArcadeLevel(string l, string c)
        {
            level = l;
            character = c;
        }
    }

    private ArcadeLevel nextLevel;
    private string player;

    // Use this for initialization
    void Start () {
        matchesWon = 0;
        inArcade = false;
        nextFightSet = false;
        returnToMain = false;

        levels = new List<ArcadeLevel>()
        {
            new ArcadeLevel("SALevel", "ScarletAce"),
            new ArcadeLevel("MSLevel", "MysticSapphire"),
            new ArcadeLevel("DestrukTorrLevel", "DestrukTorr"),
            new ArcadeLevel("KingMaximumLevel", "KingMaximum"),
            new ArcadeLevel("RustLevel", "Rust"),
            new ArcadeLevel("ST02Level", "ST-02"),
            new ArcadeLevel("SwiftShadowLevel", "SwiftShadow"),
            new ArcadeLevel("TheKnightotaurLevel", "TheKnightotaur")
        };

	}
	
	// Update is called once per frame
	void Update () {
		if(SceneManager.GetActiveScene().name == "ArcadeMode" && !inArcade)
        {
            inArcade = true;

            LoadNextFight();
        }

        else if (SceneManager.GetActiveScene().name != "ArcadeMode" && inArcade)
        {
            inArcade = false;
        }

        else if (SceneManager.GetActiveScene().name == "ArcadeMode" && inArcade)
        {
            if(Input.GetButtonUp("Start"))
            {
                //LoadNextFight();
                if (returnToMain)
                {
                    GameObject.FindGameObjectWithTag("BlackFade").GetComponent<FadeToBlack>().StartFading("MainMenu");
                    Destroy(GameObject.FindGameObjectWithTag("LoaderInfo"));
                    Destroy(this.gameObject);
                }
                else
                {
                    GameObject.FindGameObjectWithTag("BlackFade").GetComponent<FadeToBlack>().StartFading(nextLevel.level);
                }
                    
            }
        }

        if(SceneManager.GetActiveScene().name == "CharacterSelect")
        {
            if (!GameObject.FindGameObjectWithTag("LoaderInfo").GetComponent<SelectManager>().ArcadeMode)
            {
                GameObject.FindGameObjectWithTag("LoaderInfo").GetComponent<SelectManager>().SetArcade();
            }
                
        }

        /*if (SceneManager.GetActiveScene().name == "1PCharacterSelect" && !nextFightSet)
        {
            GameObject loader = GameObject.FindGameObjectWithTag("LoaderInfo");
            loader.GetComponent<SelectManager>().SetPlayerTwo(FindNextFight());
            nextFightSet = true;
        }*/
    }

    public void selectArcade()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void LoadNextFight()
    {
        if(levels.Count > 0)
        {
            nextLevel = FindNextFight();
            GameObject loader = GameObject.FindGameObjectWithTag("LoaderInfo");
            loader.GetComponent<SelectManager>().SetPlayerTwo(nextLevel.character);
            player = loader.GetComponent<SelectManager>().GetPlayerOne();
            //SceneManager.LoadScene(nextLevel.level);
            GameObject.FindGameObjectWithTag("PortraitHandler").GetComponent<ArcadePortraitManager>().DisplayPortraits(player, nextLevel.character);
        }

        else
        {
            GameObject.FindGameObjectWithTag("PortraitHandler").GetComponent<ArcadePortraitManager>().DisplayWinScreen();
            returnToMain = true;
            /*SceneManager.LoadScene("MainMenu");
            Destroy(GameObject.FindGameObjectWithTag("LoaderInfo"));
            Destroy(this.gameObject);*/
        }
    }

    private ArcadeLevel FindNextFight()
    {
        int next = Random.Range(0, levels.Count);

        ArcadeLevel al = levels[next];
        levels.RemoveAt(next);
        return al;
    }
}
