﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterLoader : MonoBehaviour {

    [System.Serializable]
    public struct playerPrefabs
    {
        public string name;
        public GameObject obj;
    }

    public playerPrefabs[] prefabs;
    private Dictionary<string, GameObject> characters = new Dictionary<string, GameObject>();
    private GameObject player1;
    private GameObject player2;
    public Vector3 p1Spawn;
    public Vector3 p2Spawn;
    public Image bar1;
    public Image bar2;
    public GameObject ko;

    public FocusCamera fcam;

    private RoundTracker rt;

    // Use this for initialization
    void Start () {
		for(int i = 0; i < prefabs.Length; i++)
        {
            characters.Add(prefabs[i].name, prefabs[i].obj);
        }

        rt = gameObject.GetComponent<RoundTracker>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadCharacters(string p1, string p2, bool arcadeMode)
    {
        GameObject firstPlayer = Instantiate(characters[p1]) as GameObject;
        GameObject secondPlayer = Instantiate(characters[p2]) as GameObject;

        firstPlayer.transform.position = p1Spawn;
        secondPlayer.transform.position = p2Spawn;

        firstPlayer.GetComponent<Player>().SetOpponent(secondPlayer.GetComponent<Player>());
        secondPlayer.GetComponent<Player>().SetOpponent(firstPlayer.GetComponent<Player>());

        firstPlayer.GetComponent<Player>().bar = bar1;
        secondPlayer.GetComponent<Player>().bar = bar2;

        firstPlayer.GetComponent<Player>().koText = ko;
        secondPlayer.GetComponent<Player>().koText = ko;

        firstPlayer.GetComponent<Player>().isPlayer2 = false;
        secondPlayer.GetComponent<Player>().isPlayer2 = true;

        fcam.player1 = firstPlayer.transform;
        fcam.player2 = secondPlayer.transform;

        if(firstPlayer.name == secondPlayer.name)
        {
            secondPlayer.GetComponent<Player>().ChangeCostume(1);
        }

        if (arcadeMode)
        {
            secondPlayer.GetComponent<Player>().ActivateAI();
        }

        rt.p1 = firstPlayer.GetComponent<Player>();
        rt.p2 = secondPlayer.GetComponent<Player>();
        rt.DisplayText();
    }
}
