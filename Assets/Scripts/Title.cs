﻿using UnityEngine;
using System.Collections;

public class Title : MonoBehaviour {

    public GameObject[] sprotes;
    private int currentPos;
    private bool currentlyMoving;
    public int startY;
    private Vector3 goal;

	// Use this for initialization
	void Start () {
        goal = new Vector3(this.transform.position.x, this.transform.position.y - ((Screen.height*2) * startY ), this.transform.position.z);
        currentlyMoving = false;
        currentPos = -1;
	}
	
	// Update is called once per frame
	void Update () {
        //sprotes[0].transform.position = Vector3.MoveTowards(sprotes[0].transform.position, goal, 200.0f * Time.deltaTime);
        if (!currentlyMoving)
        {
            int pos = Random.Range(0, sprotes.Length);
            while(pos == currentPos)
                pos = Random.Range(0, sprotes.Length);
            if ((sprotes[pos].transform.position.y > goal.y && startY == 1) || (sprotes[pos].transform.position.y < goal.y && startY == -1))
            {
                currentlyMoving = true;
                currentPos = pos;
            }
        }
        
        if (currentlyMoving)
        {
            sprotes[currentPos].transform.position = Vector3.MoveTowards(sprotes[currentPos].transform.position, goal, 200.0f * Time.deltaTime);
        }

        if ((sprotes[currentPos].transform.position.y <= goal.y && startY == 1) || (sprotes[currentPos].transform.position.y >= goal.y && startY == -1))
        {
            currentlyMoving = false;
            sprotes[currentPos].transform.position = this.transform.position;
        }
    }
}
