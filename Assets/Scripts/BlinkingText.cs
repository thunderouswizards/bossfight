﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BlinkingText : MonoBehaviour {

    public bool changeScene;
    public FadeToBlack black;
    public string sceneName;

    int count;
	Text text;
    private bool fadeTrack;

	
	// Use this for initialization
	void Start () {
		count = 60;
		text = GetComponent<Text> ();
        fadeTrack = false;
	}
	
	// Update is called once per frame
	void Update () {
		count--;
		if (count == 0) {
			text.enabled = !text.enabled;
			count = 60;
		}


        if (changeScene)
        {
            if (Input.GetButtonUp("Start") && !fadeTrack)
            {
                //SceneManager.LoadScene(sceneName);
                black.StartFading(sceneName);

                GameObject[] disable = GameObject.FindGameObjectsWithTag("Selecter");
                for(int i = 0; i < disable.Length; i++)
                {
                    disable[i].GetComponent<CharacterSelectCursor>().enabled = false;
                }
            }
        }
        
	}
	
}
