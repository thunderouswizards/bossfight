﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuCursor : MonoBehaviour
{
    Vector3 velocity;
    float velocityXSmoothing;
    int moveSpeed = 200;
    int optionHighlighted;
    public RectTransform[] selectSquares;
    public RectTransform pointer;
    public GameObject[] optionDescript;

    private string horizontal = "Horizontal";
    private string vertical = "Vertical";
    private string keyH = "KeyH";
    private string keyV = "KeyV";
    private string fire = "Fire";
    private string jump = "Jump";

    // Use this for initialization
    void Start()
    {
        moveSpeed = (int)((moveSpeed / 1024.0f) * Screen.width);
        optionHighlighted = -1;
    }

    // Update is called once per frame
    void Update()
    {

        CheckInButton();

        Vector2 input = new Vector2(Input.GetAxisRaw(horizontal), Input.GetAxisRaw(vertical));
        if (input.x == 0 && input.y == 0)
        {
            input = new Vector2(Input.GetAxisRaw(keyH), Input.GetAxisRaw(keyV));
        }

        velocity.x = input.x * moveSpeed;
        velocity.y = input.y * moveSpeed;

        transform.Translate(velocity * Time.deltaTime);

        if (Input.GetButtonUp(jump) && optionHighlighted != -1)
        {
            if(optionHighlighted == 0)
            {
                Application.LoadLevel("CharacterSelect");
            }

            else if (optionHighlighted == 1)
            {
                Application.LoadLevel("CharacterSelect");
            }

            else if (optionHighlighted == 2)
            {
                Application.Quit();
            }
        }
    }

    void CheckInButton()
    {
        float width, height, left, right, top, bottom;
        float selectX, selectY;

        float pointerW = pointer.rect.width;
        pointerW = (pointerW / 1024.0f) * Screen.width;

        float pointerH = pointer.rect.height;
        pointerH = (pointerH / 600.0f) * Screen.height;

        selectX = transform.position.x + pointerW / 2.0f;
        selectY = transform.position.y + pointerH / 2.0f;

        for (int i = 0; i < selectSquares.Length; i++)
        {
            width = selectSquares[i].rect.width;
            height = selectSquares[i].rect.height;

            width = (width / 1024.0f) * Screen.width;
            height = (height / 600.0f) * Screen.height;

            left = selectSquares[i].position.x - width / 2.0f;
            right = selectSquares[i].position.x + width / 2.0f;
            top = selectSquares[i].position.y + height / 2.0f;
            bottom = selectSquares[i].position.y - height / 2.0f;

            if (selectX > left && selectX < right)
            {
                if (selectY < top && selectY > bottom)
                {
                    optionHighlighted = i;
                    optionDescript[i].SetActive(true);
                }
            }
            if (selectX < left || selectX > right || selectY > top || selectY < bottom)
            {
                if (optionHighlighted == i)
                {
                    optionHighlighted = -1;
                    optionDescript[i].SetActive(false);
                }
            }
        }
    }
}
