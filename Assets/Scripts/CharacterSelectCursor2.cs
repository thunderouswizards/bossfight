﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterSelectCursor2 : MonoBehaviour {
	
	Vector3 velocity;
	float velocityXSmoothing;
	int moveSpeed = 200;
	bool canMove;
	int characterHighlighted;
	int characterSelected;
	public RectTransform[] selectSquares;
	public CharacterSelectManager manager;
    public SelectManager charactersPicked;

    // Use this for initialization
    void Start () {
		characterSelected = -1;
		characterHighlighted = -1;
		canMove = true;
	}
	
	// Update is called once per frame
	void Update () {
		
		CheckInCharacter ();
		
		if (canMove) {
			Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal2"), Input.GetAxisRaw ("Vertical2"));
			
			velocity.x = input.x * moveSpeed;
			velocity.y = input.y * moveSpeed;
			
			transform.Translate (velocity * Time.deltaTime);
		}
		
		if (Input.GetButtonUp ("Fire2") && characterHighlighted != -1) {
			canMove = false;
			characterSelected = characterHighlighted;
			manager.SetPlayerTwo (characterSelected);
            charactersPicked.SetPlayerTwo(characterSelected);
        }
		if (Input.GetButtonUp ("Jump2") && !canMove) {
			canMove = true;
			characterSelected = -1;
			manager.SetPlayerTwo (characterSelected);
		}
	}
	
	void CheckInCharacter(){
		float width, height, left, right, top, bottom;
		for (int i = 0; i < selectSquares.Length; i++) {
			width = selectSquares[i].rect.width;
			height = selectSquares[i].rect.height;
			
			left = selectSquares[i].position.x - width/2.0f;
			right = left + width;
			top = selectSquares[i].position.y + height/2.0f;
			bottom = top - height;
			
			if(transform.position.x > left && transform.position.x < right){
				if(transform.position.y < top && transform.position.y > bottom){
					characterHighlighted = i;
				}
			}
		}
	}
}
