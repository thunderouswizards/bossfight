﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSAIBumper : AIBumper {

    private new void OnTriggerEnter2D(Collider2D col)
    {
        int chance = Random.Range(1, 10);
        SSAI ai = (SSAI)GetAI();

        if (col.gameObject.tag == "Projectile")
        {
            if (col.gameObject.GetComponent<BulletScript>().owner != ai.gameObject && chance <= ai.GetRandomChance())
            {
                int altfire = Random.Range(1, 3);
                if (altfire == 1)
                    ai.StartDash();
                else
                    ai.Teleport();
            }
        }
                
        if (col.gameObject.tag == "Player" && ai.GetBot().isDasher && chance <= ai.GetRandomChance())
        {
            if (col.gameObject != ai.gameObject)
                ai.StartDash();
        }
    }
}
