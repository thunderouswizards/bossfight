﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Controller2D))]
public class KMAI : AI
{

    private float RandomBehavior;

    private bool charging;
    private int chargeCount;
    private int chargeUntil;

    private KMWeapon kmw;

    new void Start()
    {
        base.Start();
        RandomBehavior = 0;
        shootLimit = 1;
        chargeCount = 900; //Max number of ChargeCount is 900, one charge takes 90 frames to be available, 10 charges total.

        Random.InitState(System.DateTime.Now.Second); //Seeds random depending on current second at start.
        chargeUntil = 90 * Random.Range(0, 10); //Picks 0, 90, 180, 270, 360, 450, 540, 630, 720, or 810. Will pick one of these again when chargeCount <= chargeUntil
        kmw = gameObject.GetComponent<KMWeapon>();
    }

    void Update()
    {

        //Adjust timers to move or attack
        if (Time.timeScale == 1.0f)
        {
            timer++;
        }

        if (charging)
        {
            chargeCount--;
            if (chargeCount <= chargeUntil)
            {
                //Explode
                chargeCount = 900;
                chargeUntil = 90 * Random.Range(0, 10);
                charging = false;
                state = IDLE;
                WillAttack = true;
                gameObject.GetComponent<KMWeapon>().shootForAi();
            }
        }


        switch (state)
        {
            case IDLE:
                if (timer >= 120)
                {
                    if (!WillAttack)
                    {
                        timer = 0;
                        if (OppToRight())
                            state = MOVERIGHT;
                        if (!OppToRight())
                            state = MOVELEFT;
                    }

                    else if (WillAttack)
                    {
                        timer = 0;
                        if (!charging)
                        {
                            state = SHOOT;
                            WillAttack = false;
                        }
                    }

                    timer = 0;
                }
                break;

            case MOVERIGHT:
                if (ShouldJump())
                    break;

                if (!OppToRight())
                {
                    state = MOVELEFT;
                }

                if (timer >= 60)
                {
                    state = IDLE;
                    if (charging)
                        WillAttack = false;
                    else
                        WillAttack = true;
                    timer = 0;
                }
                break;

            case MOVELEFT:
                if (ShouldJump())
                    break;

                if (OppToRight())
                {
                    state = MOVERIGHT;
                }

                if (timer >= 60)
                {
                    state = IDLE;
                    if (charging)
                        WillAttack = false;
                    else
                        WillAttack = true;
                    timer = 0;
                }

                break;

            case JUMP:
                if (OppToRight() || GetBot().controller.collisions.left)
                {
                    state = MOVERIGHT;
                }
                else if (OppToRight() || GetBot().controller.collisions.right)
                {
                    state = MOVELEFT;
                }
                else
                    state = IDLE;

                break;

            case DASH:
                if (GetBot().dashCoolDown <= 0 && timer > 10)
                {
                    state = IDLE;
                    timer = 0;
                }
                break;
            case SHOOT:
                state = IDLE;
                timer = 0;
                charging = true;
                kmw.startCharging();

                break;
            case ALTFIRE:
                //Y'all know what it is
                state = IDLE;
                timer = 0;
                break;
        }
    }

    public void Deflect()
    {
        if(state != ALTFIRE && kmw.GetAbsorbWindow() <= 0)
        {
            state = ALTFIRE;
        }
            
    }

}
