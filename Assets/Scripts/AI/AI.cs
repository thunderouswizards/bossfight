﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI: MonoBehaviour{

    private Vector3 currentPos;
    private Vector3 opponentPos;

    private int RandomChance = 3;

    public string state = "";
    public const string IDLE = "IDLE";
    public const string DASH = "DASH";
    public const string SHOOT = "SHOOT";
    public const string JUMP = "JUMP";
    public const string ALTFIRE = "ALTFIRE";
    public const string MOVERIGHT = "MOVERIGHT";
    public const string MOVELEFT = "MOVELEFT";

    public bool grounded;
    public int shootNum;
    public int shootLimit;
    private Player Bot;
    public int timer;

    public bool WillAttack;

    // Use this for initialization
    public void Start()
    {
        state = IDLE;
        Bot = this.GetComponent<Player>();
        if (Bot.IsGrounded())
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
        shootNum = 0;
        WillAttack = false;
        timer = 0;

    }

    public virtual bool MoveRight()
    {

        if (state == MOVERIGHT)
        {
            return true;
        }
        else
            return false;
    }
    public virtual bool MoveLeft()
    {

        if (state == MOVELEFT)
        {
            return true;
        }
        else
            return false;
    }
    public virtual bool Jump()
    {

        if (state == JUMP)
        {
            return true;
        }
        else
            return false;
    }

    public virtual bool Dash()
    {
        if(state == DASH)
        {
            return true;
        }
        return false;
    }
    public virtual bool Idle()
    {
        if(state == IDLE)
        {
            return true;
        }
        return false;
    }

    public virtual bool Attack()
    {
        Vector2 current = new Vector2(GetCurrentPos().x, GetCurrentPos().y);
        Vector2 opp = new Vector2(GetOpponentPos().x, GetOpponentPos().y);
        
        if (state == SHOOT)
        {
            if (DisBetween(current, opp) < 8)
            {
                return true;
            }
            else
            {
                state = IDLE;
                timer = 0;
            }
        }


        return false;
        
    }

    public virtual bool AltFire()
    {
        if (state == ALTFIRE)
        {
            return true;
        }
        return false;
    }


    public virtual Vector3 DashTarget()
    {
        Vector2 target = new Vector2(0, 0);

        target.y = Random.Range(-1, 2);

        if (GetBot().FacingRight())
        {
            target.x = Random.Range(0, 2);
        }

        else
        {
            target.x = Random.Range(-1, 1);
        }
        return target;
    }

    public float DisBetween(Vector2 x, Vector2 y)
    {
        float distance = Mathf.Abs((y.y - x.y) / (y.x - x.x));
        return distance;
    }

    public Player GetBot()
    {
        return Bot;
    }
    
    public Vector3 GetCurrentPos()
    {
        return Bot.gameObject.transform.position;
    }
    public Vector3 GetOpponentPos()
    {
        return Bot.opponent.gameObject.transform.position;
    }
    public bool OppToRight()
    {
        if (GetCurrentPos().x <= GetOpponentPos().x)
        {
            return true;
        }
        else
            return false;
    }

    public bool OppAbove()
    {
        if (GetCurrentPos().y < GetOpponentPos().y)
            return true;
        return false;
    }

    public bool ShouldJump()
    {
        if (GetBot().MadeContact() && GetBot().IsGrounded())
        {

            state = JUMP;
            timer = 0;
            return true;
        }

        return false;
    }
    public void StartDash()
    {
        state = DASH;
        timer = 0;
        shootNum = 0;
    }

    public void SetRandomChance(int x)
    {
        RandomChance = x;
    }

    public int GetRandomChance()
    {
        return RandomChance;
    }

}
 