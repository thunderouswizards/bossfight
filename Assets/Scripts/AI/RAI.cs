﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class RAI : AI {

    private RWeapon rw;
    private bool faceRight;
    private int stanceTimer;

	// Use this for initialization
	new void Start () {
        base.Start();
        shootLimit = 1;
        stanceTimer = Random.Range(120, 241);
        stanceTimer = 60;
        rw = gameObject.GetComponent<RWeapon>();
    }
	
	// Update is called once per frame
	void Update () {
        if(Time.timeScale == 1.0f)
        {
            timer++;
            if (stanceTimer > 0)
                stanceTimer--;
        }

        switch (state)
        {
            case IDLE:
                if (timer >= 30 && shootLimit == 1)
                {
                    if (!WillAttack)
                    {
                        timer = 0;
                        if (OppToRight())
                            state = MOVERIGHT;
                        if (!OppToRight())
                            state = MOVELEFT;
                    }

                    else if (WillAttack)
                    {
                        timer = 0;
                        if (stanceTimer == 0 && shootLimit != 6)
                        {
                            state = ALTFIRE;
                            WillAttack = false;
                        }
                        else
                        {
                            state = SHOOT;
                            WillAttack = false;
                        }
                    }

                    timer = 0;
                }
                else if (timer >= 15 && shootLimit == 6)
                {
                    state = SHOOT;
                }
                break;

            case MOVERIGHT:
                if (ShouldJump())
                    break;

                if (!OppToRight())
                {
                    state = MOVELEFT;
                }

                if (timer >= 60)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 0;
                }
                break;

            case MOVELEFT:
                if (ShouldJump())
                    break;

                if (OppToRight())
                {
                    state = MOVERIGHT;
                }

                if (timer >= 60)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 0;
                }

                break;

            case JUMP:
                if (OppToRight() || GetBot().controller.collisions.left)
                {
                    state = MOVERIGHT;
                }
                else if (!OppToRight() || GetBot().controller.collisions.right)
                {
                    state = MOVELEFT;
                }
                else
                    state = IDLE;

                break;

            case DASH:
                if (GetBot().dashCoolDown <= 0 && timer > 10)
                {
                    state = IDLE;
                    timer = 0;
                }
                break;
            case SHOOT:
                if(faceRight)
                    rw.IncrementDirection();
                else
                    rw.DecrementDirection();
                shootNum++;
                state = IDLE;
                timer = 0;

                if (shootNum >= shootLimit)
                {
                    shootNum = 0;
                    WillAttack = false;
                    if (shootLimit == 6)
                    {
                        shootLimit = 1;
                        stanceTimer = Random.Range(120, 241);
                        //stanceTimer = 60;
                    }
                }
                break;
            case ALTFIRE:
                if (GetComponent<Player>().FacingRight())
                {
                    faceRight = true;
                    rw.SetCurrentDirection(0);
                }
                else
                {
                    faceRight = false;
                    rw.SetCurrentDirection(4);
                }
                shootLimit = 6;
                timer = 0;
                state = IDLE;
                break;
        }
    }


    public void EndStance()
    {
        state = IDLE;
        shootNum = 0;
        WillAttack = false;
        if (shootLimit == 6)
        {
            shootLimit = 1;
            stanceTimer = Random.Range(120, 241);
            //stanceTimer = 60;
        }
    }
    
}
