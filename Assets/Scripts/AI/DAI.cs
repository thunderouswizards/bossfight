﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DAI : AI {

	// Use this for initialization
	new void Start () {
        base.Start();
        shootLimit = 1;
    }
	
	// Update is called once per frame
	void Update () {

        if (Time.timeScale == 1.0f)
        {
            timer++;
        }

        switch (state)
        {
            case IDLE:
                if (timer >= 30 && !WillAttack)
                {
                    timer = 0;
                    if (OppToRight())
                        state = MOVERIGHT;
                    if (!OppToRight())
                        state = MOVELEFT;
                }
                else if(timer >= 10 && WillAttack)
                {
                    Vector2 current = new Vector2(GetCurrentPos().x, GetCurrentPos().y);
                    Vector2 opp = new Vector2(GetOpponentPos().x, GetOpponentPos().y);

                    timer = 0;
                    if (DisBetween(current, opp) < 0.025)
                    {
                        state = SHOOT;
                    }
                    else
                    {
                        state = ALTFIRE;
                    }
                    WillAttack = false;
                }
                break;

            case MOVERIGHT:
                if (ShouldJump())
                    break;

                if (!OppToRight())
                {
                    state = MOVELEFT;
                }

                if (timer >= 30)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 0;
                }
                break;

            case MOVELEFT:
                if (ShouldJump())
                    break;

                if (OppToRight())
                {
                    state = MOVERIGHT;
                }

                if (timer >= 30)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 0;
                }

                break;

            case JUMP:
                if (OppToRight() || GetBot().controller.collisions.left)
                {
                    state = MOVERIGHT;
                }
                else if (!OppToRight() || GetBot().controller.collisions.right)
                {
                    state = MOVELEFT;
                }
                else
                    state = IDLE;

                break;

            case DASH:
                if (GetBot().dashCoolDown <= 0 && timer > 20)
                {
                    state = IDLE;
                    timer = 0;
                }
                break;
            case SHOOT:
                shootNum++;
                timer = 0;

                if (shootNum >= shootLimit)
                {
                    shootNum = 0;
                    WillAttack = false;
                    state = MOVELEFT;
                }
                break;
            case ALTFIRE:
                shootNum++;
                if (shootNum >= shootLimit)
                {
                    timer = 0;
                    shootNum = 0;
                    WillAttack = false;
                    state = MOVELEFT;
                }
                break;
        }

    }

    public new bool Attack()
    {
        if (state == SHOOT)
        {
            /*if (DisBetween(current, opp) < 6)
            {
                return true;
            }
            else
            {
                state = IDLE;
                timer = 0;
            }*/
            return true;
        }

        return false;
    }

}
