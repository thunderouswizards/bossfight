﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Controller2D))]
public class SAAI : AI {

    private int ChanceTimer = 0;

    new void Start()
    {
        base.Start();
        shootLimit = 3;
    }

    void Update()
    {
        /*int chance = Random.Range(1,10);
        if(chance <= 3)
        {
            timer = 120;
        }*/

        if(ChanceTimer <= 30)
            ChanceTimer++;
      
        if(ChanceTimer >= 30)
        {
            int chance = Random.Range(1, 10);
            if (chance <= 3)
            {
                timer = 120;
            }
            ChanceTimer = 0;
        }

        //Adjust timers to move or attack
        if (Time.timeScale == 1.0f)
        {
            timer++;
        }

        switch (state)
        {
            case IDLE:
                if (timer >= 120)
                {
                    if (!WillAttack)
                    {
                        timer = 0;
                        if (OppToRight())
                            state = MOVERIGHT;
                        if (!OppToRight())
                            state = MOVELEFT;
                    }

                    else if (WillAttack)
                    {
                        timer = 0;
                        if (OppAbove())
                        {
                            state = ALTFIRE;
                            WillAttack = false;
                        }
                        else
                        {
                            state = SHOOT;
                            WillAttack = false;
                        }
                    }
                    
                    timer = 0;
                }
                break;

            case MOVERIGHT:
                if (ShouldJump())
                    break;

                if (!OppToRight())
                {
                    state = MOVELEFT;
                }

                if (timer >= 60)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 0;
                }
                break;

            case MOVELEFT:
                if (ShouldJump())
                    break;

                if (OppToRight())
                {
                    state = MOVERIGHT;
                }

                if (timer >= 60)
                {
                    if (OppAbove())
                    {
                        state = ALTFIRE;
                    }
                    else
                        state = SHOOT;
                    timer = 0;
                }

                break;

            case JUMP:
                if (OppToRight() || GetBot().controller.collisions.left)
                {
                    state = MOVERIGHT;
                }
                else if (!OppToRight() || GetBot().controller.collisions.right)
                {
                    state = MOVELEFT;
                }
                else
                    state = IDLE;
                
                break;

            case DASH:
                if (GetBot().dashCoolDown <= 0 && timer > 10)
                {
                    state = IDLE;
                    timer = 0;
                }
                break;
            case SHOOT:
                shootNum++;
                state = IDLE;
                timer = 0;
                    
                if (shootNum >= shootLimit)
                {
                    shootNum = 0;
                    WillAttack = false;
                }
                break;
            case ALTFIRE:
                shootNum++;
                if (shootNum >= 1)
                {
                    state = IDLE;
                    timer = 0;
                    shootNum = 0;
                    WillAttack = false;
                }
                break;
        }
    }
    
}
