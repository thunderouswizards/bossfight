﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Controller2D))]
public class MSAI : AI {

    List<Transform> bullets;
    private bool shotActive;
    private int bCall;
    private bool inRange;

	// Use this for initialization
	new void Start () {
        base.Start();
        shootLimit = 2;
        bullets = new List<Transform>();
        shotActive = false;
        bCall = 0;
        inRange = false;

        Random.InitState(System.DateTime.Now.Second);
    }
	
	// Update is called once per frame
	void Update () {
        if(bullets.Count > 0 && bCall <= 1 && bulletStopped())
        {
            shotActive = true;
            OpponentInRange();
        }
        else if(bullets.Count == 0 && bCall >= 2)
        {
            inRange = false;
            shootNum = 0;
            shotActive = false;
            bCall = 0;
        }

        //Adjust timers to move or attack
        if (Time.timeScale == 1.0f)
        {
            timer++;
        }
        switch (state)
        {
            case IDLE:
                if (timer >= 120)
                {
                    if (!WillAttack)
                    {
                        timer = 0;
                        if (OppToRight())
                            state = MOVERIGHT;
                        if (!OppToRight())
                            state = MOVELEFT;
                    }

                    else if (WillAttack)
                    {
                        timer = 0;
                        if (shotActive && bCall <= 1)
                        {
                            state = ALTFIRE;
                            //WillAttack = true;
                        }
                        else
                        {
                            state = SHOOT;
                            //WillAttack = false;
                        }
                    }

                    timer = 0;
                }
                break;

            case MOVERIGHT:
                if (ShouldJump())
                    break;

                if (!OppToRight())
                {
                    state = MOVELEFT;
                }

                if (timer >= 30)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 90;
                }
                break;

            case MOVELEFT:
                if (ShouldJump())
                    break;

                if (OppToRight())
                {
                    state = MOVERIGHT;
                }

                if (timer >= 30)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 90;
                }

                break;

            case JUMP:
                if (OppToRight() || GetBot().controller.collisions.left)
                {
                    state = MOVERIGHT;
                }
                else if (OppToRight() || GetBot().controller.collisions.right)
                {
                    state = MOVELEFT;
                }
                else
                    state = IDLE;

                break;

            case DASH:
                if (GetBot().dashCoolDown <= 0 && timer > 10)
                {
                    state = IDLE;
                    timer = 60;
                }
                break;
            case SHOOT:
                shootNum++;
                state = IDLE;
                int x = Random.Range(0, 3);
                switch (x)
                {
                    case 0:
                        timer = 0;
                        break;
                    case 1:
                        timer = 70;
                        break;
                    case 2:
                        timer = 90;
                        break;
                    default:
                        timer = 90;
                        break;
                }

                shotActive = true;
                WillAttack = true;

                if (shootNum >= shootLimit)
                {
                    shootNum = 0;
                    WillAttack = false;
                    inRange = false;
                }
                break;
            case ALTFIRE:
                bCall++;
                timer = 0;
                WillAttack = false;
                
                if (inRange)
                {
                    state = ALTFIRE;
                }
                else
                {
                    state = IDLE;
                }
                

                break;
        }
    }

    public void AddBullet(Transform bullet)
    {
        bullets.Add(bullet);
    }

    public void RemoveBullet(Transform bullet)
    {
        bCall = 0;
        bullets.Remove(bullet);
        if(bullets.Count <= 0)
        {
            shotActive = false;
            inRange = false;
        }
    }

    bool bulletStopped()
    {
        bool r = false;
        for(int i = 0; i < bullets.Count; i++)
        {
            if (bullets[i].GetComponent<MSBullet>().IsStopped())
                r = true;
        }
        return r;
    }

    void OpponentInRange()
    {
        bool inside = false;

        for(int x = 0; x < bullets.Count; x++)
        {
            bool stopped = bullets[x].GetComponent<MSBullet>().IsStopped();

            if (stopped)
            {
                float minY = transform.position.y - transform.lossyScale.y;

                float rX = (bullets[x].position.x < transform.position.x) ? bullets[x].position.x : transform.position.x;
                float rY = (bullets[x].position.y < minY) ? bullets[x].position.y : minY;
                //float midX = bullets[x].position.x + (transform.position.x - bullets[x].position.x) / 2.0f;
                //float midY = bullets[x].position.y + (minY - bullets[x].position.y) / 2.0f;
                float dX = Mathf.Abs(transform.position.x - bullets[x].position.x);
                float dY = Mathf.Abs(minY - bullets[x].position.y);

                Rect r = new Rect(rX, rY, dX, dY);

                Vector2 bp = new Vector2(GetOpponentPos().x, GetOpponentPos().y);

                if (r.Contains(bp))
                {
                    inside = true;
                }
            }
            
        }

        if (inside)
        {
            state = ALTFIRE;
            inRange = true;
        }
        else
            inRange = false;
            
    }
}
