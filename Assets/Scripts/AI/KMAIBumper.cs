﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KMAIBumper : AIBumper {

    public new void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.tag == "Projectile")
        {
            GameObject enemy = col.gameObject.GetComponent<BulletScript>().owner;
            KMAI kmai = (KMAI)GetAI();

            if (enemy != kmai.gameObject)
            {
                float eY = enemy.transform.position.y;
                float oY = kmai.gameObject.transform.position.y;
                //If enemy in line, deflect. Otherwise, dash.
                if (eY <= oY + 0.5f && eY >= oY - 0.5f)
                    kmai.Deflect();
                else
                    kmai.StartDash();
            }
        }
    }
}
