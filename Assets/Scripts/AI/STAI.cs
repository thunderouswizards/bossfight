﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Controller2D))]
public class STAI : AI
{

    private float RandomBehavior;
    private int moveCheck;

    new void Start()
    {
        base.Start();
        RandomBehavior = 0;
        shootLimit = 3;
        
        Random.InitState(System.DateTime.Now.Second);
        SetMoveCheck();
    }

    void Update()
    {
        if(state == MOVERIGHT || state == MOVELEFT)
        {
            SetMoveCheck();
        }


        //Adjust timers to move or attack
        if (Time.timeScale == 1.0f)
        {
            timer++;
        }

        switch (state)
        {
            case IDLE:
                if (timer >= 120)
                {
                    if (!WillAttack)
                    {
                        timer = 0;
                        if (OppToRight())
                            state = MOVERIGHT;
                        if (!OppToRight())
                            state = MOVELEFT;
                    }

                    else if (WillAttack)
                    {
                        timer = 0;
                        if (shootNum < shootLimit)
                        {
                            state = ALTFIRE;
                            WillAttack = false;
                        }
                        else
                        {
                            state = SHOOT;
                            WillAttack = false;
                        }
                    }

                    timer = 0;
                }
                break;

            case MOVERIGHT:
                if (ShouldJump())
                    break;

                if (!OppToRight())
                {
                    state = MOVELEFT;
                }

                if (timer >= moveCheck)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 0;
                }
                break;

            case MOVELEFT:
                if (ShouldJump())
                    break;

                if (OppToRight())
                {
                    state = MOVERIGHT;
                }

                if (timer >= moveCheck)
                {
                    state = IDLE;
                    WillAttack = true;
                    timer = 0;
                }

                break;

            case JUMP:
                if (OppToRight() || GetBot().controller.collisions.left)
                {
                    state = MOVERIGHT;
                }
                else if (OppToRight() || GetBot().controller.collisions.right)
                {
                    state = MOVELEFT;
                }
                else
                    state = IDLE;

                break;

            case DASH:
                if (GetBot().dashCoolDown <= 0 && timer > 10)
                {
                    state = IDLE;
                    timer = 0;
                }
                break;
            case SHOOT:
                state = SHOOT;
                break;
            case ALTFIRE:
                shootNum++;
                state = IDLE;
                timer = 0;

                if (shootNum > shootLimit)
                {
                    shootNum = shootLimit;
                    WillAttack = false;
                }
                break;
        }
    }

    public void endShot()
    {
        state = IDLE;
        timer = 60;
        WillAttack = false;
    }

    public void startShoot()
    {
        shootNum--;
        if(state != SHOOT)
            state = SHOOT;
    }


    private void SetMoveCheck()
    {
        int r = Random.Range(0, 3);
        switch (r)
        {
            case 0:
                moveCheck = 30;
                break;
            case 1:
                moveCheck = 60;
                break;
            case 2:
                moveCheck = 90;
                break;
            default:
                break;
        }
    }
}
