﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBumper : MonoBehaviour {
    public GameObject AICharacter;
    private AI AIScript;

	// Use this for initialization
	void Start () {
        if(AICharacter.GetComponent<Player>().isAI)
            AIScript = AICharacter.GetComponent<AI>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D col)
    {
        int chance = Random.Range(1,10);

        if(col.gameObject.tag == "Projectile")
        {
            if(col.gameObject.GetComponent<BulletScript>().owner != AIScript.gameObject && chance <= AIScript.GetRandomChance())
                AIScript.StartDash();
        }
        if (col.gameObject.tag == "Player" && AIScript.GetBot().isDasher && chance <= AIScript.GetRandomChance())
        {
            if (col.gameObject != AIScript.gameObject)
                AIScript.StartDash();
        }
    }

    public AI GetAI()
    {
        return AIScript;
    }
}
