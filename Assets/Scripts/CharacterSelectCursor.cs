﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelectCursor : MonoBehaviour {

    public CanvasScaler scaler;

    public bool isPlayer2 = false;
	Vector3 velocity;
	float velocityXSmoothing;
	public int moveSpeed = 200;
	bool canMove;
	int characterHighlighted;
	int characterSelected;
	public RectTransform[] selectSquares;
    public RectTransform pointer;
	public CharacterSelectManager manager;
    public SelectManager charactersPicked;
	public Image selected;
    public GameObject[] characterPortraits;
    public GameObject[] characterNames;
    public RectTransform backButton;

    private string horizontal = "Horizontal";
    private string vertical = "Vertical";
    private string keyH = "KeyH";
    private string keyV = "KeyV";
    private string fire = "Fire";
    private string jump = "Jump";

    private AudioSource sound;

	// Use this for initialization
	void Start () {
        moveSpeed = (int)((moveSpeed / 1024.0f) * Screen.width);
		characterSelected = -1;
		characterHighlighted = -1;
		canMove = true;

        if (isPlayer2)
        {
            horizontal = "Horizontal2";
            vertical = "Vertical2";
            keyH = "KeyH2";
            keyV = "KeyV2";
            fire = "Fire2";
            jump = "Jump2";
        }

        sound = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

		CheckInCharacter ();

		if (canMove) {
			Vector2 input = new Vector2 (Input.GetAxisRaw (horizontal), Input.GetAxisRaw (vertical));
            if(input.x == 0 && input.y == 0)
            {
                input = new Vector2(Input.GetAxisRaw(keyH), Input.GetAxisRaw(keyV));
            }

            velocity.x = input.x * moveSpeed;
			velocity.y = input.y * moveSpeed;

			transform.Translate (velocity * Time.deltaTime);

            if (transform.position.x + (pointer.rect.width / 2.0f) > Screen.width)
            {
                transform.position = new Vector3(Screen.width - (pointer.rect.width / 2.0f), transform.position.y);
            }
            else if (transform.position.x - (pointer.rect.width / 2.0f) < 0.0f)
            {
                transform.position = new Vector3(pointer.rect.width / 2.0f, transform.position.y);
            }

            if (transform.position.y + (pointer.rect.height / 2.0f) > Screen.height)
            {
                transform.position = new Vector3(transform.position.x, Screen.height - (pointer.rect.height / 2.0f));
            }
            else if (transform.position.y - (pointer.rect.height / 2.0f) < 0.0f)
            {
                transform.position = new Vector3(transform.position.x, pointer.rect.height / 2.0f);
            }
        }

		if (Input.GetButtonUp (jump) && characterHighlighted != -1)
        {
            if(canMove)
                sound.Play();
			canMove = false;
			characterSelected = characterHighlighted;
            if (isPlayer2)
            {
                manager.SetPlayerTwo(characterSelected);
                charactersPicked.SetPlayerTwo(characterSelected);
            }
            else
            {
                manager.SetPlayerOne(characterSelected);
                charactersPicked.SetPlayerOne(characterSelected);
            }
            
		}
		if (Input.GetButtonUp (fire) && !canMove) {
			canMove = true;
			characterSelected = -1;
            if(isPlayer2)
			    manager.SetPlayerTwo(characterSelected);
            else
                manager.SetPlayerOne(characterSelected);
        }
        if(Input.GetButtonUp(jump) && characterHighlighted == -1)
        {
            if (InBackButton())
            {
                if (GameObject.FindGameObjectWithTag("ArcadeManager") != null)
                    Destroy(GameObject.FindGameObjectWithTag("ArcadeManager"));

                SceneManager.LoadScene("MainMenu");
            }
        }

        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }
    }

	void CheckInCharacter(){
		float width, height, left, right, top, bottom;
        float selectX, selectY;

        float pointerW = pointer.rect.width;
        pointerW = (pointerW / scaler.referenceResolution.x) * Screen.width;

        float pointerH = pointer.rect.height;
        pointerH = (pointerH / scaler.referenceResolution.y) * Screen.height;
        
        selectX = (transform.localPosition.x / scaler.referenceResolution.x) * Screen.width + pointerW/2.0f;
        selectY = (transform.localPosition.y / scaler.referenceResolution.y) * Screen.height + pointerH/2.0f;

        for (int i = 0; i < selectSquares.Length; i++) {
			width = selectSquares[i].rect.width;
			height = selectSquares[i].rect.height;
            
            width = (width/ scaler.referenceResolution.x) * Screen.width;
            height = (height/ scaler.referenceResolution.y) * Screen.height;

            float xPos = selectSquares[i].localPosition.x / scaler.referenceResolution.x * Screen.width;
            float yPos = selectSquares[i].localPosition.y / scaler.referenceResolution.y * Screen.height;

            left = xPos - width/2.0f;
			right = xPos + width/2.0f;
			top = yPos + height/2.0f;
			bottom = yPos - height/2.0f;

            if ((selectX >= left && selectX <= right) && (selectY <= top && selectY >= bottom)) //Check if X of top-right of cursor is in the box
            {
                if(characterHighlighted != -1)
                {
                    characterPortraits[characterHighlighted].SetActive(false);
                    characterNames[characterHighlighted+1].SetActive(false);
                }

                characterHighlighted = i;
                characterPortraits[i].SetActive(true);
                characterNames[0].SetActive(false);
                characterNames[i + 1].SetActive(true);
            }
            if((selectX < left || selectX > right) || (selectY > top || selectY < bottom))
            {
                if (characterHighlighted == i)
                {
                    characterHighlighted = -1;
                    characterPortraits[i].SetActive(false);
                    characterNames[i+1].SetActive(false);
                    characterNames[0].SetActive(true);
                }
            }
        }
	}

    public bool InBackButton()
    {
        float width, height, left, right, top, bottom;
        float selectX, selectY;

        float pointerW = pointer.rect.width;
        pointerW = (pointerW / scaler.referenceResolution.x) * Screen.width;

        float pointerH = pointer.rect.height;
        pointerH = (pointerH / scaler.referenceResolution.y) * Screen.height;

        selectX = (transform.localPosition.x / scaler.referenceResolution.x) * Screen.width + pointerW / 2.0f;
        selectY = (transform.localPosition.y / scaler.referenceResolution.y) * Screen.height + pointerH / 2.0f;

        width = backButton.rect.width;
        height = backButton.rect.height;

        width = (width / scaler.referenceResolution.x) * Screen.width;
        height = (height / scaler.referenceResolution.y) * Screen.height;

        float xPos = backButton.localPosition.x / scaler.referenceResolution.x * Screen.width;
        float yPos = backButton.localPosition.y / scaler.referenceResolution.y * Screen.height;

        left = xPos - width / 2.0f;
        right = xPos + width / 2.0f;
        top = yPos + height / 2.0f;
        bottom = yPos - height / 2.0f;

        if ((selectX >= left && selectX <= right) && (selectY <= top && selectY >= bottom))
        {
            return true;
        }
        
        return false;
    }
}
