﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour {

    private Image fade;
    private string nextScene;
    private bool beginFade;

	// Use this for initialization
	void Start () {
        fade = gameObject.GetComponent<Image>();
        beginFade = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (beginFade)
        {
            fade.color = new Color(fade.color.r, fade.color.g, fade.color.b, fade.color.a + 0.2f);
            if (fade.color.a >= 1)
            {
                SceneManager.LoadScene(nextScene);
            }
        }
	}

    public void StartFading(string scene)
    {
        if (!beginFade)
        {
            beginFade = true;
            nextScene = scene;
        }
    }
}
