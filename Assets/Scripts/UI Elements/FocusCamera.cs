﻿using UnityEngine;
using System.Collections;

public class FocusCamera : MonoBehaviour {
	public Transform player1;
	public Transform player2;

    public float speed = 1.0f;

	public float minSizeY = 5.0f;
	public Camera cam;
    // Use this for initialization


    //THIS'LL BE COOL, I SWEAR
    public Transform camTransform;

    // How long the object should shake for.
    public float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0.5f;
    public float decreaseFactor = 1.0f;

    Vector3 originalPos; //starting position of camera
    float originalZ; //camera's starting z
    float relativeValue; //The relative value of the camera's z starting comapred to the players' distance from each other
    float relativeY; //The relavtive value of the camera's starting z compared to the y distance from the player to the top of the screen, assuming the player is at the top

    void Start () {
        GameObject csm = GameObject.Find("WhatToLoad");
		SelectManager s = csm.GetComponent<SelectManager>();

		GameObject[] all = GameObject.FindGameObjectsWithTag("Player");

		for(int i = 0; i < all.Length; i++)
		{
			if(all[i].activeSelf && !player1)
			{
				player1 = all[i].transform;
			}

			else if(all[i].activeSelf && !player2)
			{
				player2 = all[i].transform;
			}
		}

        cam = GetComponent<Camera>();

        //float xDif = 32.0f; //distance between two players
        float xDif = Vector3.Distance(new Vector3(-10.0f, 5.0f), new Vector3(10.0f, 5.0f));
        float yDif = 4.0f;
        originalZ = transform.position.z;
        relativeValue = originalZ / xDif; //camera z compared to player distances
        relativeY = originalZ / yDif; //camera z compared to minimum allowed distance between player and top of screen
    }

    float FindRelativeZ()
    {
        //float currentXDif = Mathf.Abs(player1.position.x - player2.position.x); //current x distance between players
        float currentXDif = Vector3.Distance(player1.position, player2.position);
        
        float newZ = currentXDif * relativeValue; //New z value to maintain the relative value of z compared to player differences

        if (newZ <= originalZ)
        {
            return newZ;
        }
        return transform.position.z;
    }

	void SetCameraPos() 
	{
		Vector3 middle;
        
		if(player1 != null && player2 != null)
		{
            middle = ((player1.position + player2.position) * 0.5f) * 0.75f;

            float newZ = FindRelativeZ();

            Vector3 goal = new Vector3(middle.x, middle.y, newZ);

            float step = speed * Time.deltaTime;

            


            //transform.position = new Vector3(middle.x, middle.y + 2.5f, newZ);
            transform.position = Vector3.Lerp(transform.position, goal, step);
        }
        //originalPos = cam.transform.localPosition;
    }

	void ScaleStage()
	{
		float minSizeX = minSizeY * (Screen.width / Screen.height);

		float width = Mathf.Abs(player1.position.x - player2.position.x) * 0.5f;
		float height = Mathf.Abs(player1.position.y + - player2.position.y) * 0.75f;

		float camSizeX = Mathf.Max(width, minSizeX);
		cam.orthographicSize = Mathf.Max(height, camSizeX * Screen.height / Screen.width, minSizeY);
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (shakeDuration > 0)
        {
            cam.transform.position += Random.insideUnitSphere * shakeAmount;
            cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, cam.transform.position.z);
            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            SetCameraPos();
            //ScaleStage();
            //cam.transform.localPosition = originalPos;
        }
    }

    /*void ShakeStage()
    {
        if (shakeDuration > 0)
        {
            cam.transform.localPosition += Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            SetCameraPos();
            ScaleStage();
            //cam.transform.localPosition = originalPos;
        }
    }*/

}
