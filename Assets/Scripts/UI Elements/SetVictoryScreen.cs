﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetVictoryScreen : MonoBehaviour {

    private string[][] AllWinQuotes;

    public GameObject[] playerWon;
    public GameObject[] portraitWon;
    public GameObject[] quoteWon;
    private Text winquote;
    private string typeOut;
    private int quoteIndex;
    private int typeWait;

    private bool typeText;

	// Use this for initialization
	void Start () {
        typeText = false;
        quoteIndex = 0;
        typeWait = 2;
        AllWinQuotes = new string[][] {
            new string[] {
                "Testing against myself was a good start! Now to move on to other Bots!",
                "Come on, Sis! We both know playing all fancy'll get you nowhere!",
                "Dude, you're being way too aggressive. How're you gonna get better if you keep whining?",
                "I mean, yeah, your Bot's shiny... but that's as impressive as it gets...",
                "Oh wow, a cowboy! I should've given Scarlet Ace a poncho or something!",
                "Wait, that's THE Knightotaur? Huh, I get why you haven't customized it now.",
                "Huh, I haven't seen that Bot before. Probably won't be seeing it again after that whoopin'.",
                "Oh man, I love that ninja look! If you can find a way to make it faster, I'd love to fight again!"
            },
            new string [] {
                "Ha! In your face, Zack! Er, I mean, your hubris lead to your own defeat.",
                "Another Mystic Sapphire, hm? Well nothing beats the original.",
                "Elegance is the key to victory. Remember that next ti- sorry, \"Azure Tears\" doesn't sound like a band I'd like.",
                "Hey, hey, don't cry. Remember, \"You are only as strong as you think you are.\"",
                "If you keep yourself in a box, you'll never improve. Also, cowboys are played out.",
                "I can already tell you're dedicated. But dedication alone doesn't equal skill.",
                "This was a playtest, right? I'd recommend some close range options. Huh? All I do is manipulate bullets? Well, uh...",
                "You are talented. You've got an interesting style. Please fight me again... Sorry if that was a little stilted."
            },
            new string []
            {
                "Oh man, I dig your whole \"basic Bot with no real talents\" thing! You ever listen to, \"Oath?\"",
                "Wow, I stomped all over you. Oh, you should check out \"Azure Tears!\" Their tunes totally go with that \"mellow and sad\" thing you've got going on.",
                "Ha, your DestrukTorr's laaaaame. Go home and feel bad about yourself. You don't even deserve a band rec.",
                "Felt pretty good putting a rich kid in his place! I'd check out "
            },
            new string []
            {

            },
            new string []
            {

            },
            new string []
            {

            },
            new string []
            {

            },
            new string []
            {

            }
        };
	}
	
	// Update is called once per frame
	void Update () {
        /*if (Input.GetButtonUp("Start"))
        {
            Application.LoadLevel("CharacterSelect");
        }*/

        if (typeText)
        {
            
            if(typeWait == 2)
            {
                winquote.text += typeOut[quoteIndex];
                quoteIndex++;
                if (quoteIndex == typeOut.Length)
                    typeText = false;
                typeWait = 0;
            }
            typeWait++;
        }
    }

    public void SetPlayerWon(int num)
    {
        playerWon[num].SetActive(true);
    }

    public void SetPortrait(int num)
    {
        portraitWon[num].SetActive(true);
        quoteWon[num].SetActive(true);
        winquote = quoteWon[num].GetComponent<Text>();
        typeOut = winquote.text;
        winquote.text = "";
        typeText = true;
    }
}