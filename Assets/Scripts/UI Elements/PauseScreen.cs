﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseScreen : MonoBehaviour {

    private RoundTracker rt;
    private Image pause;
    private Color baseColor;
    private Color seeThrough;
    private bool paused;

	// Use this for initialization
	void Start () {
        rt = GameObject.FindGameObjectWithTag("CharacterLoader").GetComponent<RoundTracker>();

        pause = this.GetComponent<Image>();
        baseColor = pause.color;
        seeThrough = baseColor;
        seeThrough.a = 0;
        pause.color = seeThrough;
        paused = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Start") && Time.timeScale != 0.25f && !rt.RoundIsDisplayed())
        {
            if (paused)
            {
                pause.color = seeThrough;
                paused = false;
            }
            else
            {
                pause.color = baseColor;
                paused = true;
            }
        }
	}
}
