﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorRotation : MonoBehaviour {

    public float angle;

    private float speed = 0.5f;
    private Vector3 left, right, target;
    private float startTime;
    private bool moveLeft;
    private float jLength;

	// Use this for initialization
	void Start () {
        left = transform.localPosition;
        left.x = left.x - 0.5f;
        right = transform.localPosition;

        target = left;

        startTime = Time.time;
        moveLeft = true;
        
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.right, angle * Time.deltaTime);

        //Move left then right.
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / 0.5f;

        transform.localPosition = Vector3.Lerp(transform.localPosition, target, fracJourney);
        if (transform.localPosition == target)
        {
            if (moveLeft)
            {
                moveLeft = false;
                target = right;
                speed = 2.5f;
            }
            else
            {
                moveLeft = true;
                target = left;
                speed = 0.5f;
            }
            startTime = Time.time;
        }
	}
}
