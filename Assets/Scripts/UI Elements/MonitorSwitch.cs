﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MonitorSwitch : MonoBehaviour {

    public FadeToBlack ftb;
    public ArcadeManager aManager;

    private Vector3[] targets;
    public List<GameObject> monitors;
    public List<GameObject> stext;
    private float[] startTimes;
    private float jLength;

    private float speed = 10.0f;

    public bool moveUp, moveDown;
    private int select;


    private string jump = "Jump";

    public GameObject controlsBack;
    private GameObject textDisplay;
    public GameObject controlsText;
    public GameObject creditsText;
    private string active, growing, inactive, shrinking;
    private string controlsState;


    // Use this for initialization
    void Start () {
        targets = new Vector3[monitors.Count];
        for(int i = 0; i < monitors.Count; i++)
        {
            targets[i] = monitors[i].transform.position;
        }
        monitors.RemoveAt(0);
        monitors.RemoveAt(monitors.Count-1);
        moveUp = false;
        moveDown = false;

        startTimes = new float[monitors.Count];
        for(int i = 0; i < startTimes.Length; i++)
        {
            startTimes[i] = Time.time;
        }
        jLength = Vector3.Distance(monitors[0].transform.position, targets[2]);
        select = 0;

        active = "ACTIVE";
        growing = "GROWING";
        inactive = "INACTIVE";
        controlsState = inactive;
        controlsState = inactive;
    }
	
	// Update is called once per frame
	void Update () {
        

        if (moveUp)
        {
            for (int i = 0; i < monitors.Count; i++)
            {
                float distCovered = (Time.time - startTimes[i]) * speed;
                float fracJourney = distCovered / jLength;

                monitors[i].transform.position = Vector3.Lerp(monitors[i].transform.position, targets[i + 2], fracJourney);
            }
            
            if (monitors[monitors.Count-1].transform.position == targets[targets.Length-1])
            {
                monitors[monitors.Count - 1].transform.position = targets[1];
                monitors.Insert(0, monitors[monitors.Count - 1]);
                monitors.RemoveAt(monitors.Count - 1);
                moveUp = false;
                

                /*for (int i = 0; i < monitors.Count; i++)
                {
                    startTimes[i] = Time.time;
                }*/
            }
        }

        else if (moveDown)
        {
            for (int i = 0; i < monitors.Count; i++)
            {
                float distCovered = (Time.time - startTimes[i]) * speed;
                float fracJourney = distCovered / jLength;

                monitors[i].transform.position = Vector3.Lerp(monitors[i].transform.position, targets[i], fracJourney);
            }

            if (monitors[0].transform.position == targets[0])
            {
                monitors[0].transform.position = targets[targets.Length-2];
                monitors.Add(monitors[0]);
                monitors.RemoveAt(0);
                moveDown = false;

                /*for (int i = 0; i < monitors.Count; i++)
                {
                    startTimes[i] = Time.time;
                }*/
            }
        }

        

        if (controlsState == growing)
        {
            Vector3 s = controlsBack.transform.localScale;
            controlsBack.transform.localScale = new Vector3(s.x + 0.1f, s.y + 0.1f, s.z + 0.1f);
            if (controlsBack.transform.localScale.x >= 1.0f)
            {
                controlsState = active;
                textDisplay.SetActive(true);
            }

        }
        else if (controlsState == active)
        {
            if (Input.GetButtonUp(jump))
            {
                //controlsBack.SetActive(false);
                textDisplay.SetActive(false);
                controlsState = shrinking;
            }
        }
        else if (controlsState == shrinking)
        {
            Vector3 s = controlsBack.transform.localScale;
            controlsBack.transform.localScale = new Vector3(s.x - 0.1f, s.y - 0.1f, s.z - 0.1f);
            if (controlsBack.transform.localScale.x <= 0.1f)
            {
                controlsState = inactive;
                textDisplay.SetActive(false);
            }
        }

        else
        {
            float input = Input.GetAxis("Vertical");
            if (input == 0.0f)
                input = Input.GetAxis("KeyV");

            if (input != 0.0f && moveUp == false && moveDown == false)
            {
                if (input > 0.0f)
                {
                    moveUp = true;
                    ChangeSelection(1);
                }
                else if (input < 0.0f)
                {
                    moveDown = true;
                    ChangeSelection(-1);
                }
                for (int i = 0; i < monitors.Count; i++)
                {
                    startTimes[i] = Time.time;
                }
            }

            if (Input.GetButtonUp(jump))
            {
                switch (select)
                {
                    /*
                     * 0 = Main Menu
                     * 1 = VS
                     * 2 = Arcade
                     * 3 = Credits
                     * 4 = Controls
                     */
                    case 0:
                        ftb.StartFading("TitleScreen");
                        break;
                    case 1:
                        ftb.StartFading("CharacterSelect");
                        break;
                    case 2:
                        aManager.selectArcade();
                        ftb.StartFading("CharacterSelect");
                        break;
                    case 3:
                        controlsBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                        controlsBack.SetActive(true);
                        textDisplay = creditsText;
                        controlsState = growing;
                        break;
                    case 4:
                        controlsBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                        controlsBack.SetActive(true);
                        textDisplay = controlsText;
                        controlsState = growing;
                        break;
                }
            }

            if (Input.GetKeyDown("escape"))
            {
                Application.Quit();
            }
        }

    }

    private void ChangeSelection(int num)
    {
        stext[select].GetComponent<ScrollingText>().ResetPos();
        stext[select].SetActive(false);
        
        select += num;
        if(select < 0)
        {
            select = 4;
        }
        else if (select > 4)
        {
            select = 0;
        }

        stext[select].SetActive(true);
    }
}
