﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingText : MonoBehaviour {

    //Just a heads up, this codes assumes the game object is a child of another that has a Mask component.

    public float speed;

    private Vector3 startPoint;
    private Vector3 endPoint;

	// Use this for initialization
	void Start () {
        startPoint = transform.localPosition;
        endPoint = startPoint;
        endPoint.x = startPoint.x * -1;
	}
	
	// Update is called once per frame
	void Update () {
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, endPoint, speed * Time.deltaTime);
        if(transform.localPosition.x == endPoint.x)
        {
            transform.localPosition = startPoint;
        }
	}

    public void ResetPos()
    {
        transform.localPosition = startPoint;
    } 
}
