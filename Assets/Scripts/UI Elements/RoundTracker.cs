﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundTracker : MonoBehaviour {

    public GameObject t;
    private int roundDisplay;
    private int fightDisplay;
    private int roundnum;

    public Player p1, p2;

	// Use this for initialization
	void Start () {
        roundDisplay = 0;
        roundnum = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if(roundDisplay > 0)
        {
            roundDisplay--;
            if(roundDisplay == 0)
            {
                fightDisplay = 30;
                t.GetComponent<Text>().text = "Fight!";
            }
        }

        if(fightDisplay > 0)
        {
            fightDisplay--;
            if(fightDisplay == 0)
            {
                t.SetActive(false);
                ChangePlayers(true);
            }
        }
	}

    public void DisplayText()
    {
        roundDisplay = 60; //Display Round number for half a second
        roundnum++;
        t.GetComponent<Text>().text = "Round " + roundnum;
        t.SetActive(true);
        ChangePlayers(false);
    }

    private void ChangePlayers(bool b)
    {
        p1.enabled = b;
        p2.enabled = b;

        if (p1.isAI)
        {
            p1.gameObject.GetComponent<AI>().enabled = b;
        }

        if (p2.isAI)
        {
            p2.gameObject.GetComponent<AI>().enabled = b;
        }
    }

    public bool RoundIsDisplayed()
    {
        if (roundDisplay > 0 || fightDisplay > 0)
            return true;
        else
            return false;
    }
}
